/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
@Repository


public class Web_GrabarMovimientosClave {
   @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    
    
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PCK_WEB_ClaveEmergencia")
                .withProcedureName("GrabarMovimientosClave")
                .declareParameters(
                        new SqlParameter("v_id_reclamo", Types.INTEGER),
                        new SqlParameter("v_numero_contrato", Types.VARCHAR),
                        new SqlParameter("v_codigo_suscripcion", Types.INTEGER),
                        new SqlParameter("v_codigo_tipo_servicio", Types.VARCHAR),
                        new SqlParameter("v_codigo_medico", Types.VARCHAR),
                        new SqlParameter("v_codigo_clinica", Types.VARCHAR),
                        new SqlParameter("v_fecha_ocurrencia", Types.VARCHAR),
                        new SqlParameter("v_status_reclamo", Types.VARCHAR),
                        new SqlParameter("v_comentarios", Types.VARCHAR),
                        new SqlParameter("v_status_aprobacion", Types.VARCHAR),
                        new SqlParameter("v_codigo_intervencion", Types.VARCHAR),
                        new SqlParameter("v_descripcion_intervencion", Types.VARCHAR),
                        new SqlParameter("v_costo_estimado", Types.INTEGER),
                        new SqlParameter("v_registros", Types.INTEGER),
                        new SqlParameter("v_numero_aval_clave", Types.VARCHAR),
                        new SqlParameter("v_sub_clasificacion", Types.VARCHAR),
                        new SqlParameter("v_monto_facturado", Types.INTEGER),
                        new SqlParameter("v_usuario", Types.VARCHAR),
                        new SqlParameter("v_fecha_modificacion", Types.VARCHAR),
                                               
                        
                    new SqlOutParameter("mensaje11", Types.VARCHAR)
                );
    
       
}
    public String grabarmovimientosclave (String j_id_reclamo,String j_numero_contrato, String j_codigo_suscripcion ,
                                   String j_codigo_tipo_servicio, String j_codigo_medico, String j_codigo_clinica,String j_fecha_ocurrencia,	 
                                   String j_status_reclamo, String j_comentarios,String j_status_aprobacion,String j_codigo_intervencion,	 
                                   String j_descripcion_intervencion, String j_costo_estimado, String j_registros,
                                   String v_numero_aval_clave,String v_sub_clasificacion,String v_monto_facturado,String v_usuario,String v_fecha_modificacion)
    
    {
       
       // (String jcedula,String jcontrato) {
        
      // map<Integer, object> result=  simpleJdbcCall.execute(jcedula.trim());
       Map<String, Object> result = simpleJdbcCall.execute(j_id_reclamo.trim(),j_numero_contrato.trim(), j_codigo_suscripcion.trim() ,
                                                            j_codigo_tipo_servicio.trim(), j_codigo_medico.trim(), j_codigo_clinica.trim(),j_fecha_ocurrencia.trim(),	 
                                                            j_status_reclamo.trim(), j_comentarios.trim(),j_status_aprobacion.trim(),j_codigo_intervencion.trim(),	 
                                                            j_descripcion_intervencion.trim(), j_costo_estimado.trim(), j_registros.trim(), 
                                                            v_numero_aval_clave.trim() ,v_sub_clasificacion.trim() ,v_monto_facturado.trim() 
                                                            ,v_usuario.trim() ,v_fecha_modificacion.trim() );
       String resultado = (String) result.get("mensaje11");
       return resultado;
    }  
}
