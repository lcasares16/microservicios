/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
@Repository


public class Web_AutenticarUsuario {
      @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    
    
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PCK_WEB_LOGIN")
                .withProcedureName("AutenticarUsuario")
                .declareParameters(
                    new SqlParameter("V_eLogin", Types.VARCHAR),
                    new SqlParameter("V_eClaveUsuario", Types.VARCHAR),  
                    new SqlParameter("V_PLATAFORMA", Types.VARCHAR), 
                    new SqlOutParameter("mensaje11", Types.VARCHAR)
                );
    
}
    public String autenticarusuario2(String V_elogin,String V_eclaveusuario,String v_plataforma) {
            
      // map<Integer, object> result=  simpleJdbcCall.execute(jcedula.trim());
       Map<String, Object> result = simpleJdbcCall.execute(V_elogin.trim(),
                                                           V_eclaveusuario.trim(),
                                                           v_plataforma.trim()
                                                            );
                                                          
       String resultado = (String) result.get("mensaje11");
       return resultado;
    }
    
    
}
