/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo.procedimientos;

import java.io.Serializable;

/**
 *
 * @author admoracle
 */

public class Stp_ListadoClavesEmergencia {
         private String ID_RECLAMO;
         private String SUB_CLASIFICACION;
         private String STATUS;
         private String CEDULA;
         private String NOMBRE;
         private String MONTO_FACTURADO;
         private String OBSERVACIONES_AUDITADO;
         
         
            
         
         
         
         
         public Stp_ListadoClavesEmergencia () {}
    
    public String getID_RECLAMO() {
        return ID_RECLAMO;
    }

      public void setID_RECLAMO(String ID_RECLAMO) {
        this.ID_RECLAMO = ID_RECLAMO;
       
    }
        
       public String getSUB_CLASIFICACION() {
        return SUB_CLASIFICACION;
    }

 
    public void setSUB_CLASIFICACION(String SUB_CLASIFICACION) {
        this.SUB_CLASIFICACION = SUB_CLASIFICACION;
    }
    
    
       public String getCEDULA() {
        return CEDULA;
    }

        public void setCEDULA(String CEDULA) {
        this.CEDULA = CEDULA;
    }
    
    
        public String getNOMBRE() {
        return NOMBRE;
    }

        public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }    
       
        public String getOBSERVACIONES_AUDITADO() {
        return OBSERVACIONES_AUDITADO;
    }

        public void setOBSERVACIONES_AUDITADO(String OBSERVACIONES_AUDITADO) {
        this.OBSERVACIONES_AUDITADO = OBSERVACIONES_AUDITADO;
    } 
        
        
        
       public String getSTATUS() {
        return STATUS;
    }

       public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
      
    
    public String getMONTO_FACTURADO() {
        return MONTO_FACTURADO;
    }

 
    public void setMONTO_FACTURADO(String MONTO_FACTURADO) {
        this.MONTO_FACTURADO = MONTO_FACTURADO;
    }
         
         
         
         
}
