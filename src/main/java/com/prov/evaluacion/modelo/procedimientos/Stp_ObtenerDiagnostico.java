/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo.procedimientos;

import java.io.Serializable;

/**
 *
 * @author admoracle
 */

public class Stp_ObtenerDiagnostico {
    
         private String CODIGO;
         private String DESCRIPCION;
    
         
         
          public Stp_ObtenerDiagnostico () {}
    
    public String getCODIGO() {
        return CODIGO;
    }

      public void setCODIGO(String CODIGO) {
        this.CODIGO = CODIGO;
       
    }
      
      public String getDESCRIPCION() {
        return DESCRIPCION;
    }

      public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
       
    }
         
}
