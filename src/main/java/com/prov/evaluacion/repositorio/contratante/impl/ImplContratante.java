/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.contratante.impl;

import com.prov.evaluacion.repositorio.contratante.servicio.InterfContratante;
import com.prov.evaluacion.modelo.Contratante;
import com.prov.evaluacion.repositorio.contratante.servicio.InterfContratante;
import com.prov.evaluacion.repositorio.contratante.RepoContratante;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author roberto.espinoza
 */


@Service
@Transactional("OracletransactionManager")

public class ImplContratante implements InterfContratante {
    
    @Autowired
    private RepoContratante repoContratante;
    
    @Override
     public Contratante buscarPorContrato (String contrato) {
         return repoContratante.findByNumeroContrato(contrato);
     }
     
    @Override
    public Contratante salvarContratante (Contratante contratante) {
         return repoContratante.save(contratante);
    }
    
    @Override
    public Contratante ultimoContrato () {
         return repoContratante.ultimoContrato();
    }
    
}
