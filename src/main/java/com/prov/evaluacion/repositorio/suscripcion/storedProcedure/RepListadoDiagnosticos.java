/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
@Repository
 



/**
 *
 * @author admoracle
 */
public class RepListadoDiagnosticos {
         @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    
    
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("pck_consult_genera_clave")
                .withProcedureName("listadodiagnosticos")
                .declareParameters(
                   new SqlParameter("V_CONTRATO", Types.VARCHAR),
                    new SqlParameter("V_SUSCRIPCION", Types.VARCHAR),  
                    new SqlOutParameter("mensaje11", Types.VARCHAR)
                );
    
}
    public String listadodiagnosticos(String V_CONTRATO,String V_SUSCRIPCION) {
            
      // map<Integer, object> result=  simpleJdbcCall.execute(v_contrato.trim());
       Map<String, Object> result = simpleJdbcCall.execute(V_CONTRATO.trim(),
                                                           V_SUSCRIPCION.trim());
       String resultado = (String) result.get("mensaje11");
       return resultado;
    }
    
}
