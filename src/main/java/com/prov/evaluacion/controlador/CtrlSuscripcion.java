/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.controlador;

import com.prov.evaluacion.DbContextHolder;
import com.prov.evaluacion.DbType;
import com.prov.evaluacion.modelo.Contratante;
import com.prov.evaluacion.modelo.RespuestaStandard;
import com.prov.evaluacion.modelo.procedimientos.Stp_pruebaRoberto;
import com.prov.evaluacion.modelo.Suscripcion;
import com.prov.evaluacion.modelo.procedimientos.StpListadoFacturasContratantes;
import com.prov.evaluacion.modelo.procedimientos.Stp_ListadoClavesAutorizadas;
import com.prov.evaluacion.modelo.procedimientos.Stp_ListadoClavesEmergencia;
import com.prov.evaluacion.modelo.procedimientos.Stp_ObtenerDiagnostico;
import com.prov.evaluacion.modelo.procedimientos.Stp_listadoBaremoaps;
/*import com.prov.evaluacion.modelo.procedimientos.Stpclaves2;*/
import com.prov.evaluacion.repositorio.contratante.servicio.InterfContratante;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoSpSuscripcion;
import java.math.BigInteger;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.prov.evaluacion.repositorio.suscripcion.RepoSuscripcion;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepCodigoArea;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepConsultaRemesa;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepCreaSecuenciaRemesa;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepDatosFacturasContratantes;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepGenerarClaveFinal;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepGenerarRemesa;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoAutenticarUsuario;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoValidarTitular;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoDatosContratanteCobertura;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoListadoAfiliados;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepListadoDiagnosticos;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepObtenerMensajeDia;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoCambiarClave;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoClavesListadoClaves;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoListadoBaremoAPS;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.RepoListadoFacturasContratantes;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ActualizaReclamoExtension;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ActualizarReclamo;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ValidarTitularConsulta;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_DatosContratanteCoberturaConsulta;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ObtenerClasificacionClinica;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ValidarTitular;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ListadoAfiliadosConsulta;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_AutenticarUsuario;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_DatosContratanteCobertura;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_DatosReclamoEgreso;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_DatosReclamoExtension;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_DatosReclamoIngreso;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_GenerarClave;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_GenerarCompromiso;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_GenerarNumeroAvalClave;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_GrabarMovimientosAPS;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_GrabarMovimientosClave;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_GrabarReclamo;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_GrabarRegistrosDocumentos;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_InsertaJson;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ListadoAfiliados;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ListadoClasificacion;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ListadoClavesAutorizadas;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ListadoClavesEmergencia;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ObtenerDiagnostico;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ObtenerEmailAdministrador;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ObtenerTipoCoberturaContrato;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_PEncripta;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ValidarCumuloApsPB;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ValidarCumuloApsPBAP;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ValidarCumuloApsPBAPE;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ValidarCumuloApsPE;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ValidarCumuloApsPEAP;
import com.prov.evaluacion.repositorio.suscripcion.storedProcedure.Web_ValidarCumuloApsPEAPE;
import javax.persistence.EntityManager;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author roberto.espinoza
 */

@RestController
public class CtrlSuscripcion {

    @Autowired
    private RepoSpSuscripcion repoSpSuscripcion;
    
    @Autowired
    private RepoAutenticarUsuario repoAutenticarUsuario;
    
    @Autowired
    private RepoSuscripcion repoSuscripcion;
    
    @Autowired
    private RepoValidarTitular RepoValidarTitular;
    
       @Autowired
    private RepoCambiarClave RepoCambiarClave;
    
    @Autowired
    private RepoDatosContratanteCobertura RepoDatosContratanteCobertura;
    
     @Autowired
    private RepoListadoAfiliados RepoListadoAfiliados;
    
        @Autowired
    private RepListadoDiagnosticos RepListadoDiagnosticos;
    
    @Autowired
    private RepGenerarClaveFinal RepGenerarClaveFinal;
    
    
        
    @Autowired
    private RepoClavesListadoClaves RepoClavesListadoClaves;
    
    
        @Autowired
    private RepDatosFacturasContratantes RepDatosFacturasContratantes;
    
    
      @Autowired
    private RepoListadoFacturasContratantes RepoListadoFacturasContratantes;
      
      @Autowired
    private RepCreaSecuenciaRemesa RepCreaSecuenciaRemesa;  
    
      
         @Autowired
    private RepGenerarRemesa RepGenerarRemesa;  
      
            @Autowired
    private RepConsultaRemesa RepConsultaRemesa;  
            
               @Autowired
    private RepObtenerMensajeDia RepObtenerMensajeDia; 
               
     @Autowired
    private RepCodigoArea RepCodigoArea;            
               
     
     
      @Autowired
    private RepoListadoBaremoAPS RepoListadoBaremoAPS; 
     
     @Autowired
    private Web_AutenticarUsuario Web_AutenticarUsuario; 
      
       @Autowired
    private Web_ObtenerClasificacionClinica Web_ObtenerClasificacionClinica; 
      
        @Autowired
    private Web_ValidarTitularConsulta Web_ValidarTitularConsulta; 
             
  
       @Autowired
    private Web_DatosContratanteCoberturaConsulta Web_DatosContratanteCoberturaConsulta;  
        
         @Autowired
    private Web_ListadoAfiliadosConsulta Web_ListadoAfiliadosConsulta;   
       
          @Autowired
    private Web_ListadoAfiliados Web_ListadoAfiliados; 
      
         
         
         @Autowired
    private Web_ValidarTitular Web_ValidarTitular;  
         
     @Autowired
    private Web_DatosContratanteCobertura Web_DatosContratanteCobertura;      
     
      @Autowired
    private Web_ObtenerDiagnostico Web_ObtenerDiagnostico;   
     
      @Autowired
    private Web_ObtenerTipoCoberturaContrato Web_ObtenerTipoCoberturaContrato; 
     
      
      @Autowired
    private Web_GrabarReclamo Web_GrabarReclamo; 
      
      @Autowired
    private Web_GenerarClave Web_GenerarClave;
      
      @Autowired
    private Web_ValidarCumuloApsPB Web_ValidarCumuloApsPB;
      
        @Autowired
    private Web_ValidarCumuloApsPBAP Web_ValidarCumuloApsPBAP;
      
         @Autowired
    private Web_ValidarCumuloApsPE Web_ValidarCumuloApsPE;
      
         
       @Autowired
    private Web_ValidarCumuloApsPBAPE Web_ValidarCumuloApsPBAPE;  
         
        @Autowired
    private Web_ValidarCumuloApsPEAP Web_ValidarCumuloApsPEAP;   
        
        
       @Autowired
    private Web_ValidarCumuloApsPEAPE Web_ValidarCumuloApsPEAPE; 
       
       
      @Autowired
    private Web_GrabarMovimientosAPS Web_GrabarMovimientosAPS;        
       
       @Autowired
    private Web_GrabarRegistrosDocumentos Web_GrabarRegistrosDocumentos;  
      
       
       @Autowired
    private Web_PEncripta Web_PEncripta;  
       
       
         @Autowired
    private Web_InsertaJson Web_InsertaJson;  
      
              @Autowired
    private Web_GenerarNumeroAvalClave Web_GenerarNumeroAvalClave; 
         
    @Autowired
    private Web_DatosReclamoIngreso Web_DatosReclamoIngreso;      
      
    @Autowired
    private Web_ActualizarReclamo Web_ActualizarReclamo;  
    
    @Autowired
    private Web_GrabarMovimientosClave Web_GrabarMovimientosClave;  
    
    
     @Autowired
    private Web_ListadoClavesAutorizadas Web_ListadoClavesAutorizadas;  
     
     
     @Autowired
    private Web_DatosReclamoEgreso Web_DatosReclamoEgreso; 
    
    
       @Autowired
    private Web_ListadoClasificacion Web_ListadoClasificacion;   
       
       @Autowired
    private Web_DatosReclamoExtension Web_DatosReclamoExtension; 
       
      @Autowired
    private Web_ActualizaReclamoExtension Web_ActualizaReclamoExtension;  
       
      @Autowired
    private Web_GenerarCompromiso Web_GenerarCompromiso; 
      
         @Autowired
    private Web_ListadoClavesEmergencia Web_ListadoClavesEmergencia; 
      
         @Autowired
    private Web_ObtenerEmailAdministrador Web_ObtenerEmailAdministrador;
         
         
               
               
    @Autowired
    EntityManager entityManager;
    
    
        //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/Remesa/GenerarRemesa/{v_id_reclamo}/{v_id_remesa}/{v_links}/{v_archivo}", method = RequestMethod.GET, produces = "application/json")
    public String generarremesa(@PathVariable("v_id_reclamo") final String v_id_reclamo,
                                      @PathVariable("v_id_remesa") final String v_id_remesa,
                                      @PathVariable("v_links") final String v_links,
                                      @PathVariable("v_archivo") final String v_archivo) {
     return RepGenerarRemesa.generarremesa(v_id_reclamo,v_id_remesa,v_links,v_archivo);
     } 
    
    
          //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/ClaveEmergencia/DatosReclamoIngreso/{v_eclave}/{v_ecodigoclinica}/{v_codigo_tipo_servicio}", method = RequestMethod.GET, produces = "application/json")
    public String datosreclamoingreso(@PathVariable("v_eclave") final String v_eclave,
                                      @PathVariable("v_ecodigoclinica") final String v_ecodigoclinica,
                                      @PathVariable("v_codigo_tipo_servicio") final String v_codigo_tipo_servicio
                                      ) {
     return Web_DatosReclamoIngreso.datosreclamoingreso(v_eclave,v_ecodigoclinica,v_codigo_tipo_servicio);
     }
    
    
    
           //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/ClaveEmergencia/DatosReclamoExtension/{v_eclave}/{v_ecodigoclinica}/{v_codigo_tipo_servicio}", method = RequestMethod.GET, produces = "application/json")
    public String datosreclamoextension(@PathVariable("v_eclave") final String v_eclave,
                                      @PathVariable("v_ecodigoclinica") final String v_ecodigoclinica,
                                      @PathVariable("v_codigo_tipo_servicio") final String v_codigo_tipo_servicio
                                      ) {
     return Web_DatosReclamoExtension.datosreclamoextension(v_eclave,v_ecodigoclinica,v_codigo_tipo_servicio);
     }
    
           //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/ClaveEmergencia/DatosReclamoEgreso/{v_eclave}/{v_ecodigoclinica}/{v_codigo_tipo_servicio}", method = RequestMethod.GET, produces = "application/json")
    public String datosreclamoegreso(@PathVariable("v_eclave") final String v_eclave,
                                      @PathVariable("v_ecodigoclinica") final String v_ecodigoclinica,
                                      @PathVariable("v_codigo_tipo_servicio") final String v_codigo_tipo_servicio
                                      ) {
     return Web_DatosReclamoEgreso.datosreclamoegreso(v_eclave,v_ecodigoclinica,v_codigo_tipo_servicio);
     }
    
    
    
    
          //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/ClaveEmergencia/listadoclasificacion/{v_codigo_tipo_servicio}", method = RequestMethod.GET, produces = "application/json")
    public String listadoclasificacion(@PathVariable("v_codigo_tipo_servicio") final String v_codigo_tipo_servicio
                                       ) {
     return Web_ListadoClasificacion.listadoclasificacion(v_codigo_tipo_servicio);
     }
    
    
    
    
      @RequestMapping(value = "/GenerarClaveAps/GenerarDocumentoCompromiso/{v_reclamo}/{v_clinica}/{v_medico}", method = RequestMethod.GET, produces = "application/json")
    public String generarcompromiso(@PathVariable("v_reclamo") final String v_reclamo,
                                      @PathVariable("v_clinica") final String v_clinica,
                                      @PathVariable("v_medico") final String v_medico) {
    
     return Web_GenerarCompromiso.generarcompromiso(v_reclamo,v_clinica,v_medico);
              
         }
    
   
    @RequestMapping(value = "/Login/ObtenerEmailAdministrador/{v_clinica}", method = RequestMethod.GET, produces = "application/json")
    public String obteneremailadministrador(@PathVariable("v_clinica") final String v_clinica) {
    
    return Web_ObtenerEmailAdministrador.obteneremailadministrador(v_clinica);
              
     }
    
    
    
    
           //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/Remesa/ConsultaRemesa/{v_id_reclamo}/{v_id_remesa}", method = RequestMethod.GET, produces = "application/json")
    public String consultaremesa(@PathVariable("v_id_reclamo") final String v_id_reclamo,
                                 @PathVariable("v_id_remesa") final String v_id_remesa){
     return RepConsultaRemesa.consultaremesa(v_id_reclamo,v_id_remesa );
     } 
    
    
       
    
    
    
    @RequestMapping(value = "/Remesa/creasecuenciaremesa", method = RequestMethod.GET, produces = "application/json")
    public String creasecuenciaremesa() {
    
    return RepCreaSecuenciaRemesa.creasecuenciaremesa();
                     
     }  
    
   
     @RequestMapping(value = "/ClaveAPS/GenerarClave", method = RequestMethod.GET, produces = "application/json")
    public String generarclave() {
    
    return Web_GenerarClave.generarclave();
                     
     }  
    
     @RequestMapping(value = "/ClaveEmergencia/GenerarNumeroAvalClave", method = RequestMethod.GET, produces = "application/json")
    public String generarnumeroavalclave() {
    
    return Web_GenerarNumeroAvalClave.generarnumeroavalclave();
                     
     }  
    
    
     @GetMapping(path = "/ClaveAPS/ObtenerDiagnostico" )
      public List<Stp_ObtenerDiagnostico> suscripcion2(){
       
        return Web_ObtenerDiagnostico.obtenerdiagnostico();
    }  
    
    
    
    @RequestMapping(value = "/Login/ObtenerMensajeDia", method = RequestMethod.GET, produces = "application/json")
    public String obtenermensajedia() {
    
    return RepObtenerMensajeDia.obtenermensajedia();
                     
     } 
    
    
      @RequestMapping(value = "/Login/CodigoArea/{v_codigo}", method = RequestMethod.GET, produces = "application/json")
    public String codigoarea(@PathVariable("v_codigo") final String v_codigo) {
    
    return RepCodigoArea.codigoarea(v_codigo);
                     
     } 
    
    
    @RequestMapping(value = "/Remesa/DatosFacturasContratantes/{v_clinica}/{v_contrato}", method = RequestMethod.GET, produces = "application/json")
    public String datosfacturascontratantes(@PathVariable("v_clinica") final String v_clinica,
                                             @PathVariable("v_contrato") final String v_contrato) {
    
    return RepDatosFacturasContratantes.datosfacturascontratantes(v_clinica,v_contrato);
                     
     }  
    
    
    @RequestMapping(value = "/PCK_WEB_LOGIN/P_ENCRIPTA/{vtxt}", method = RequestMethod.GET, produces = "application/json")
    public String pencripta(@PathVariable("vtxt") final String vtxt) {
    
    return Web_PEncripta.pencripta(vtxt);
                     
     }
     
    
    @RequestMapping(value = "/PCK_WEB_LOGIN/INSERTA_JSON/{v_codigo_suscripcion}/{v_numero_contrato}/{v_fecha}/{vjson}", method = RequestMethod.GET, produces = "application/json")
    public String insertajson(@PathVariable("v_codigo_suscripcion") final String v_codigo_suscripcion,
                            @PathVariable("v_numero_contrato") final String v_numero_contrato,
                            @PathVariable("v_fecha") final String v_fecha,
                            @PathVariable("vjson") final String vjson
                            ) {
    
    return Web_InsertaJson.insertajson(v_codigo_suscripcion,v_numero_contrato,v_fecha, vjson);
                     
     }
    
    
    
    
       @GetMapping(path = "/Remesa/ListadoFacturasContratantes/{v_clinica}" )
      public List<StpListadoFacturasContratantes> suscripcion(@PathVariable(value = "v_clinica") final String certificado){
        BigInteger cert = new BigInteger(certificado);
        return RepoListadoFacturasContratantes.listadofacturascontratantes(cert);
    }
    
    
    
      
     @GetMapping(path = "/ClaveAPS/ListadoBaremoAPS/{v_clinica}" )
      public List<Stp_listadoBaremoaps> suscripcion1(@PathVariable(value = "v_clinica") final String certificado){
        BigInteger cert = new BigInteger(certificado);
        return RepoListadoBaremoAPS.listadoBaremoaps(cert);
    }  
      
      @GetMapping(path = "/ClaveEmergencia/ListadoClavesAutorizadas/{v_clinica}" )
      public List<Stp_ListadoClavesAutorizadas> suscripcion11(@PathVariable(value = "v_clinica") final String certificado){
        return Web_ListadoClavesAutorizadas.listadoclavesautorizadas(certificado);
      }  
      
      
       @GetMapping(path = "/ClaveEmergencia/ListadoClavesEmergencia/{v_clinica}" )
       public List<Stp_ListadoClavesEmergencia> suscripcion12(@PathVariable(value = "v_clinica") final String certificado){
       return Web_ListadoClavesEmergencia.listadoclavesemergencia(certificado);
      }  
      
      
      
   
      
   
        //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/Claves/ListadoClaves/{v_clinica}/{v_fechaini}/{v_fechafin}/{v_status}", method = RequestMethod.GET, produces = "application/json")
    public String claveslistadoclaves(@PathVariable("v_clinica") final String v_clinica,
                                      @PathVariable("v_fechaini") final String v_fechaini,
                                      @PathVariable("v_fechafin") final String v_fechafin, 
                                      @PathVariable("v_status") final String v_status) {
    
     return RepoClavesListadoClaves.claveslistadoclaves(v_clinica,v_fechaini,v_fechafin,v_status);
              
         }   
    
    
       @RequestMapping(value = "/Login/AutenticarUsuario/{v_elogin}/{v_eclaveusuario}/{v_plataforma}", method = RequestMethod.GET, produces = "application/json")
    public String autenticarusuario2(@PathVariable("v_elogin") final String v_elogin,
                                      @PathVariable("v_eclaveusuario") final String v_eclaveusuario,
                                      @PathVariable("v_plataforma") final String v_plataforma 
                                      ) {
    
     return Web_AutenticarUsuario.autenticarusuario2(v_elogin, v_eclaveusuario, v_plataforma);
              
         }  
    
    
        @RequestMapping(value = "/Login/ObtenerClasificacionClinica/{v_clinica}", method = RequestMethod.GET, produces = "application/json")
    public String obtenerclasificacionclinica(@PathVariable("v_clinica") final String v_clinica
                                      
                                      ) {
    
     return Web_ObtenerClasificacionClinica.obtenerclasificacionclinica(v_clinica);
              
         }
    
    
       @RequestMapping(value = "/ConsultaAfiliado/ValidarTitularConsulta/{vcedulatitular}", method = RequestMethod.GET, produces = "application/json")
    public String validartitularconsulta(@PathVariable("vcedulatitular") final String vcedulatitular
                                      
                                      ) {
    
     return Web_ValidarTitularConsulta.validartitularconsulta(vcedulatitular);
              
         }
    
    
       @RequestMapping(value = "/ConsultaAfiliado/DatosContratanteCoberturaConsulta/{vcedulatitular}", method = RequestMethod.GET, produces = "application/json")
    public String datoscontratantecoberturaconsulta(@PathVariable("vcedulatitular") final String vcedulatitular
                                      
                                      ) {
    
     return Web_DatosContratanteCoberturaConsulta.datoscontratantecoberturaconsulta(vcedulatitular);
              
         }
    
    
    
       @RequestMapping(value = "/ConsultaAfiliado/ListadoAfiliadosConsulta/{vcedulatitular}/{vcodigoservicio}/{vnumerocontrato}", method = RequestMethod.GET, produces = "application/json")
    public String listadoafiliadosconsulta(@PathVariable("vcedulatitular") final String vcedulatitular,
                                      @PathVariable("vcodigoservicio") final String vcodigoservicio,
                                      @PathVariable("vnumerocontrato") final String vnumerocontrato 
                                      ) {
    
     return Web_ListadoAfiliadosConsulta.listadoafiliadosconsulta(vcedulatitular, vcodigoservicio, vnumerocontrato);
              
         }  
    
    
     @RequestMapping(value = "/ClaveAPS/ValidarTitular/{vcedulatitular}/{vcodigoservicio}", method = RequestMethod.GET, produces = "application/json")
    public String validartitular(@PathVariable("vcedulatitular") final String vcedulatitular,
                                      @PathVariable("vcodigoservicio") final String vcodigoservicio
                                      ) {
    
     return Web_ValidarTitular.validartitular(vcedulatitular, vcodigoservicio );
              
         } 
    
    
    
     @RequestMapping(value = "/ClaveAPS/DatosContratanteCobertura/{vcedulatitular}/{vcodigoservicio}/{vnumerocontrato}", method = RequestMethod.GET, produces = "application/json")
    public String datoscontratantecobertura(@PathVariable("vcedulatitular") final String vcedulatitular,
                                      @PathVariable("vcodigoservicio") final String vcodigoservicio,
                                      @PathVariable("vnumerocontrato") final String vnumerocontrato 
                                      ) {
    
     return Web_DatosContratanteCobertura.datoscontratantecobertura(vcedulatitular, vcodigoservicio, vnumerocontrato);
              
         } 
    
    
     @RequestMapping(value = "/ClaveAPS/ListadoAfiliados/{vcedulatitular}/{vcodigoservicio}/{vnumerocontrato}", method = RequestMethod.GET, produces = "application/json")
    public String listadoafiliados(@PathVariable("vcedulatitular") final String vcedulatitular,
                                      @PathVariable("vcodigoservicio") final String vcodigoservicio,
                                      @PathVariable("vnumerocontrato") final String vnumerocontrato 
                                      ) {
    
     return Web_ListadoAfiliados.listadoafiliados(vcedulatitular, vcodigoservicio, vnumerocontrato);
              
         } 
    
    
    
     @RequestMapping(value = "/ClaveAPS/ValidarCumuloApsPB/{V_eCodigoSuscripcion}/{V_sMtoCoberturaAPS}/{V_eMontoTotalPantalla}/{v_eNumeroContrato}", method = RequestMethod.GET, produces = "application/json")
    public String validarcumuloapspb(@PathVariable("V_eCodigoSuscripcion") final String V_eCodigoSuscripcion,
                                      @PathVariable("V_sMtoCoberturaAPS") final String V_sMtoCoberturaAPS,
                                      @PathVariable("V_eMontoTotalPantalla") final String V_eMontoTotalPantalla,
                                      @PathVariable("v_eNumeroContrato") final String v_eNumeroContrato 
                                      ) {
    
     return Web_ValidarCumuloApsPB.validarcumuloapspb(V_eCodigoSuscripcion, V_sMtoCoberturaAPS, V_eMontoTotalPantalla,v_eNumeroContrato);
              
         } 
    
      @RequestMapping(value = "/ClaveAPS/ValidarCumuloApsPBAP/{V_eCodigoSuscripcion}/{V_sMtoCoberturaAPS}/{V_eMontoTotalPantalla}/{v_eNumeroContrato}", method = RequestMethod.GET, produces = "application/json")
    public String validarcumuloapspbap(@PathVariable("V_eCodigoSuscripcion") final String V_eCodigoSuscripcion,
                                      @PathVariable("V_sMtoCoberturaAPS") final String V_sMtoCoberturaAPS,
                                      @PathVariable("V_eMontoTotalPantalla") final String V_eMontoTotalPantalla,
                                      @PathVariable("v_eNumeroContrato") final String v_eNumeroContrato 
                                      ) {
    
     return Web_ValidarCumuloApsPBAP.validarcumuloapspbap(V_eCodigoSuscripcion, V_sMtoCoberturaAPS, V_eMontoTotalPantalla,v_eNumeroContrato);
              
         } 
    
    
    
    @RequestMapping(value = "/ClaveAPS/ValidarCumuloApsPE/{V_eCodigoSuscripcion}/{V_sMtoCoberturaAPS}/{V_eMontoTotalPantalla}/{v_eNumeroContrato}/{V_eCodigoIntervencion}", method = RequestMethod.GET, produces = "application/json")
    public String validarcumuloapspe(@PathVariable("V_eCodigoSuscripcion") final String V_eCodigoSuscripcion,
                                      @PathVariable("V_sMtoCoberturaAPS") final String V_sMtoCoberturaAPS,
                                      @PathVariable("V_eMontoTotalPantalla") final String V_eMontoTotalPantalla,
                                      @PathVariable("v_eNumeroContrato") final String v_eNumeroContrato,
                                      @PathVariable("V_eCodigoIntervencion") final String V_eCodigoIntervencion
                                      
                                      ) {
    
     return Web_ValidarCumuloApsPE.validarcumuloapspe(V_eCodigoSuscripcion, V_sMtoCoberturaAPS, V_eMontoTotalPantalla,v_eNumeroContrato,V_eCodigoIntervencion);
              
         } 
    
    
    
    
    
     @RequestMapping(value = "/ClaveAPS/ValidarCumuloApsPBAPE/{V_eCodigoSuscripcion}/{V_sMtoCoberturaAPS}/{V_eMontoTotalPantalla}/{v_eNumeroContrato}", method = RequestMethod.GET, produces = "application/json")
    public String validarcumuloapspbape(@PathVariable("V_eCodigoSuscripcion") final String V_eCodigoSuscripcion,
                                      @PathVariable("V_sMtoCoberturaAPS") final String V_sMtoCoberturaAPS,
                                      @PathVariable("V_eMontoTotalPantalla") final String V_eMontoTotalPantalla,
                                      @PathVariable("v_eNumeroContrato") final String v_eNumeroContrato 
                                      ) {
    
     return Web_ValidarCumuloApsPBAPE.validarcumuloapspbape(V_eCodigoSuscripcion, V_sMtoCoberturaAPS, V_eMontoTotalPantalla,v_eNumeroContrato);
              
         } 
    
    @RequestMapping(value = "/ClaveAPS/ValidarCumuloApsPEAP/{V_eCodigoSuscripcion}/{V_sMtoCoberturaAPS}/{V_eMontoTotalPantalla}/{v_eNumeroContrato}/{V_eCodigoIntervencion}", method = RequestMethod.GET, produces = "application/json")
    public String validarcumuloapspeap(@PathVariable("V_eCodigoSuscripcion") final String V_eCodigoSuscripcion,
                                      @PathVariable("V_sMtoCoberturaAPS") final String V_sMtoCoberturaAPS,
                                      @PathVariable("V_eMontoTotalPantalla") final String V_eMontoTotalPantalla,
                                      @PathVariable("v_eNumeroContrato") final String v_eNumeroContrato,
                                      @PathVariable("V_eCodigoIntervencion") final String V_eCodigoIntervencion
                                      
                                      ) {
    
     return Web_ValidarCumuloApsPEAP.validarcumuloapspeap(V_eCodigoSuscripcion, V_sMtoCoberturaAPS, V_eMontoTotalPantalla,v_eNumeroContrato,V_eCodigoIntervencion);
              
         } 
    
    
    
     @RequestMapping(value = "/ClaveAPS/ValidarCumuloApsPEAPE/{V_eCodigoSuscripcion}/{V_sMtoCoberturaAPS}/{V_eMontoTotalPantalla}/{v_eNumeroContrato}", method = RequestMethod.GET, produces = "application/json")
    public String validarcumuloapspeape(@PathVariable("V_eCodigoSuscripcion") final String V_eCodigoSuscripcion,
                                      @PathVariable("V_sMtoCoberturaAPS") final String V_sMtoCoberturaAPS,
                                      @PathVariable("V_eMontoTotalPantalla") final String V_eMontoTotalPantalla,
                                      @PathVariable("v_eNumeroContrato") final String v_eNumeroContrato 
                                      ) {
    
     return Web_ValidarCumuloApsPEAPE.validarcumuloapspeape(V_eCodigoSuscripcion, V_sMtoCoberturaAPS, V_eMontoTotalPantalla,v_eNumeroContrato);
              
         }
    
    
    
    
    
    
     @RequestMapping(value = "/ClaveAPS/ObtenerTipoCoberturaContrato/{vnumerocontrato}", method = RequestMethod.GET, produces = "application/json")
    public String obtenertipocoberturacontrato(@PathVariable("vnumerocontrato") final String vnumerocontrato 
                                      ) {
    
     return Web_ObtenerTipoCoberturaContrato.obtenertipocoberturacontrato(vnumerocontrato);
              
         } 
    
    
    
    
    
        
     @RequestMapping(value = "/ClaveAPS/GrabarReclamo/{v_idreclamo}/{v_total_facturado}/{v_ecodigosuscripcion}/{v_ecodigotiposervicio}/{v_codigo_medico}/{v_codigo_clinica}/{v_status}/{v_contacto_clinica}/{v_comentarios}/{v_ecodareacelular}/{v_ecelular}/{v_efechaocurrencia}/{v_numero_contrato}/{v_codigo_intervencion}/{v_gastos_clinica}/{v_monto_facturado}/{v_autorizado}/{v_clasificacion_e}", method = RequestMethod.GET, produces = "application/json")
    public String grabarreclamo(@PathVariable("v_idreclamo") final String v_idreclamo,
                                      @PathVariable("v_total_facturado") final String v_total_facturado,
                                      @PathVariable("v_ecodigosuscripcion") final String v_ecodigosuscripcion,
                                      @PathVariable("v_ecodigotiposervicio") final String v_ecodigotiposervicio,
                                      @PathVariable("v_codigo_medico") final String v_codigo_medico,
                                      @PathVariable("v_codigo_clinica") final String v_codigo_clinica,
                                      @PathVariable("v_status") final String v_status,
                                      @PathVariable("v_contacto_clinica") final String v_contacto_clinica,
                                      @PathVariable("v_comentarios") final String v_comentarios,
                                      @PathVariable("v_ecodareacelular") final String v_ecodareacelular,
                                      @PathVariable("v_ecelular") final String v_ecelular,
                                      @PathVariable("v_efechaocurrencia") final String v_efechaocurrencia,
                                      @PathVariable("v_numero_contrato") final String v_numero_contrato,
                                      @PathVariable("v_codigo_intervencion") final String v_codigo_intervencion,
                                      @PathVariable("v_gastos_clinica") final String v_gastos_clinica ,
                                      @PathVariable("v_monto_facturado") final String v_monto_facturado,
                                      @PathVariable("v_autorizado") final String v_autorizado,
                                      @PathVariable("v_clasificacion_e") final String v_clasificacion_e)
                                                                                        {
     return Web_GrabarReclamo.grabarreclamo(v_idreclamo ,
                                                v_total_facturado,	 
                                                v_ecodigosuscripcion ,
                                                v_ecodigotiposervicio,	 
                                                v_codigo_medico	 ,
                                                v_codigo_clinica,	 
                                                v_status,	 
                                                v_contacto_clinica ,
                                                v_comentarios	 ,
                                                v_ecodareacelular,
                                                v_ecelular,
                                                v_efechaocurrencia,	 
                                                v_numero_contrato,	 
                                                v_codigo_intervencion,
                                                v_gastos_clinica,
                                                v_monto_facturado,
                                                v_autorizado,
                                                v_clasificacion_e
                                                 );
     } 
    
    
    
    
    
    @RequestMapping(value = "/ClaveAPS/GrabarMovimientosAPS/{v_id_reclamo}/{v_numero_contrato}/{v_codigo_suscripcion}/{v_codigo_tipo_servicio}/{v_codigo_medico}/{v_codigo_clinica}/{v_fecha_ocurrencia}/{v_status_reclamo}/{v_comentarios}/{v_status_aprobacion}/{v_codigo_intervencion}/{v_descripcion_intervencion}/{v_costo_estimado}/{v_registros}/{v_codigo_especialidad}/{v_codigo_clasificacion}/{v_codigo_sub_clasifica}/{v_codigo_tipo_servicio_aps}/{v_monto_baremos}", method = RequestMethod.GET, produces = "application/json")
    public String grabarmovimientosaps(@PathVariable("v_id_reclamo") final String v_id_reclamo,
                                      @PathVariable("v_numero_contrato") final String v_numero_contrato,
                                      @PathVariable("v_codigo_suscripcion") final String v_codigo_suscripcion,
                                      @PathVariable("v_codigo_tipo_servicio") final String v_codigo_tipo_servicio,
                                      @PathVariable("v_codigo_medico") final String v_codigo_medico,
                                      @PathVariable("v_codigo_clinica") final String v_codigo_clinica,
                                      @PathVariable("v_fecha_ocurrencia") final String v_fecha_ocurrencia,
                                      @PathVariable("v_status_reclamo") final String v_status_reclamo,
                                      @PathVariable("v_comentarios") final String v_comentarios,
                                      @PathVariable("v_status_aprobacion") final String v_status_aprobacion,
                                      @PathVariable("v_codigo_intervencion") final String v_codigo_intervencion,
                                      @PathVariable("v_descripcion_intervencion") final String v_descripcion_intervencion,
                                      @PathVariable("v_costo_estimado") final String v_costo_estimado,
                                      @PathVariable("v_registros") final String v_registros ,
                                      @PathVariable("v_codigo_especialidad") final String v_codigo_especialidad ,
                                      @PathVariable("v_codigo_clasificacion") final String v_codigo_clasificacion ,
                                      @PathVariable("v_codigo_sub_clasifica") final String v_codigo_sub_clasifica ,
                                      @PathVariable("v_codigo_tipo_servicio_aps") final String v_codigo_tipo_servicio_aps ,
                                      @PathVariable("v_monto_baremos") final String v_monto_baremos) 
    
                                         {
     return Web_GrabarMovimientosAPS.grabarmovimientosaps(v_id_reclamo ,
                                                v_numero_contrato,	 
                                                v_codigo_suscripcion ,
                                                v_codigo_tipo_servicio,	 
                                                v_codigo_medico	 ,
                                                v_codigo_clinica,	 
                                                v_fecha_ocurrencia,	 
                                                v_status_reclamo ,
                                                v_comentarios	 ,
                                                v_status_aprobacion,	 
                                                v_codigo_intervencion,	 
                                                v_descripcion_intervencion,	 
                                                v_costo_estimado,
                                                v_registros,
                                                v_codigo_especialidad,
                                                v_codigo_clasificacion,
                                                v_codigo_sub_clasifica,
                                                v_codigo_tipo_servicio_aps,
                                                v_monto_baremos);
     } 
    
    
    
     @RequestMapping(value = "/ClaveEmergencia/GrabarMovimientosClave/{v_id_reclamo}/{v_numero_contrato}/{v_codigo_suscripcion}/{v_codigo_tipo_servicio}/{v_codigo_medico}/{v_codigo_clinica}/{v_fecha_ocurrencia}/{v_status_reclamo}/{v_comentarios}/{v_status_aprobacion}/{v_codigo_intervencion}/{v_descripcion_intervencion}/{v_costo_estimado}/{v_registros}/{v_numero_aval_clave}/{v_sub_clasificacion}/{v_monto_facturado}/{v_usuario}/{v_fecha_modificacion}", method = RequestMethod.GET, produces = "application/json")
    public String grabarmovimientosclave(@PathVariable("v_id_reclamo") final String v_id_reclamo,
                                      @PathVariable("v_numero_contrato") final String v_numero_contrato,
                                      @PathVariable("v_codigo_suscripcion") final String v_codigo_suscripcion,
                                      @PathVariable("v_codigo_tipo_servicio") final String v_codigo_tipo_servicio,
                                      @PathVariable("v_codigo_medico") final String v_codigo_medico,
                                      @PathVariable("v_codigo_clinica") final String v_codigo_clinica,
                                      @PathVariable("v_fecha_ocurrencia") final String v_fecha_ocurrencia,
                                      @PathVariable("v_status_reclamo") final String v_status_reclamo,
                                      @PathVariable("v_comentarios") final String v_comentarios,
                                      @PathVariable("v_status_aprobacion") final String v_status_aprobacion,
                                      @PathVariable("v_codigo_intervencion") final String v_codigo_intervencion,
                                      @PathVariable("v_descripcion_intervencion") final String v_descripcion_intervencion,
                                      @PathVariable("v_costo_estimado") final String v_costo_estimado,
                                      @PathVariable("v_registros") final String v_registros ,
                                      @PathVariable("v_numero_aval_clave") final String v_numero_aval_clave ,
                                      @PathVariable("v_sub_clasificacion") final String v_sub_clasificacion ,
                                      @PathVariable("v_monto_facturado") final String v_monto_facturado ,
                                      @PathVariable("v_usuario") final String v_usuario ,
                                      @PathVariable("v_fecha_modificacion") final String v_fecha_modificacion) 
    
                                         {
                               
                                    return Web_GrabarMovimientosClave.grabarmovimientosclave(v_id_reclamo ,
                                                v_numero_contrato,	 
                                                v_codigo_suscripcion ,
                                                v_codigo_tipo_servicio,	 
                                                v_codigo_medico	 ,
                                                v_codigo_clinica,	 
                                                v_fecha_ocurrencia,	 
                                                v_status_reclamo ,
                                                v_comentarios	 ,
                                                v_status_aprobacion,	 
                                                v_codigo_intervencion,	 
                                                v_descripcion_intervencion,	 
                                                v_costo_estimado,
                                                v_registros,
                                                v_numero_aval_clave,
                                                v_sub_clasificacion,
                                                v_monto_facturado,
                                                v_usuario,
                                                v_fecha_modificacion);
     } 
    
    
      
    
     @RequestMapping(value = "/ClaveEmergencia/GrabarRegistrosDocumentos/{v_tipo_documento}/{v_tipo_servicio}/{v_plataforma}/{v_forma}/{v_numero_contrato}/{v_certificado}/{v_cedula}/{v_codigo_suscripcion}/{v_id_reclamo}/{v_nombre_documento}", method = RequestMethod.GET, produces = "application/json")
    public String grabarregistrosdocumentos(@PathVariable("v_tipo_documento") final String v_tipo_documento,
                                      @PathVariable("v_tipo_servicio") final String v_tipo_servicio,
                                      @PathVariable("v_plataforma") final String v_plataforma,
                                      @PathVariable("v_forma") final String v_forma,
                                      @PathVariable("v_numero_contrato") final String v_numero_contrato,
                                      @PathVariable("v_certificado") final String v_certificado,
                                      @PathVariable("v_cedula") final String v_cedula,
                                      @PathVariable("v_codigo_suscripcion") final String v_codigo_suscripcion,
                                      @PathVariable("v_id_reclamo") final String v_id_reclamo,
                                      @PathVariable("v_nombre_documento") final String v_nombre_documento) {
     return Web_GrabarRegistrosDocumentos.grabarregistrosdocumentos(v_tipo_documento,
                                                        v_tipo_servicio,	 
                                                        v_plataforma ,
                                                        v_forma,	 
                                                        v_numero_contrato	 ,
                                                        v_certificado,	 
                                                        v_cedula,	 
                                                        v_codigo_suscripcion ,
                                                        v_id_reclamo	 ,
                                                        v_nombre_documento);
     } 
    
    
    
    
    
    
    
     @RequestMapping(value = "/ClaveEmergencia/ActualizarReclamo/{v_eidreclamo}/{v_emontofactura}/{v_ecodigosuscripcion}/{v_ecodigotiposervicio}/{v_ecodigointervencion}/{v_ediagnostico}/{v_ecodigomedico}/{v_ecodigoclinica}/{v_estatus}/{v_econtactoclinica}/{v_eobservaciones}/{v_ecodareacelular}/{v_ecelular}/{v_efechaocurrencia}/{v_enumerocontrato}/{v_clasificacion}/{v_subclasificacion}", method = RequestMethod.GET, produces = "application/json")
    public String datosreclamoingreso(@PathVariable("v_eidreclamo") final String v_eidreclamo,
                                      @PathVariable("v_emontofactura") final String v_emontofactura,
                                      @PathVariable("v_ecodigosuscripcion") final String v_ecodigosuscripcion,
                                      @PathVariable("v_ecodigotiposervicio") final String v_ecodigotiposervicio,
                                      @PathVariable("v_ecodigointervencion") final String v_ecodigointervencion,
                                      @PathVariable("v_ediagnostico") final String v_ediagnostico,
                                      @PathVariable("v_ecodigomedico") final String v_ecodigomedico,
                                      @PathVariable("v_ecodigoclinica") final String v_ecodigoclinica,
                                      @PathVariable("v_estatus") final String v_estatus,
                                      @PathVariable("v_econtactoclinica") final String v_econtactoclinica,
                                      @PathVariable("v_eobservaciones") final String v_eobservaciones,
                                      @PathVariable("v_ecodareacelular") final String v_ecodareacelular,
                                      @PathVariable("v_ecelular") final String v_ecelular,
                                      @PathVariable("v_efechaocurrencia") final String v_efechaocurrencia ,
                                      @PathVariable("v_enumerocontrato") final String v_enumerocontrato ,
                                      @PathVariable("v_clasificacion") final String v_clasificacion ,
                                      @PathVariable("v_subclasificacion") final String v_subclasificacion 
                                         )
                                                                                        {
     return Web_ActualizarReclamo.datosreclamoingreso(v_eidreclamo ,
                                                v_emontofactura ,
                                                v_ecodigosuscripcion,	 
                                                v_ecodigotiposervicio	 ,
                                                v_ecodigointervencion,	 
                                                v_ediagnostico,	 
                                                v_ecodigomedico ,
                                                v_ecodigoclinica	 ,
                                                v_estatus,
                                                v_econtactoclinica,
                                                v_eobservaciones,	 
                                                v_ecodareacelular,	 
                                                v_ecelular,
                                                v_efechaocurrencia,
                                                v_enumerocontrato,
                                                v_clasificacion,
                                                v_subclasificacion 
                                                 );
     } 
    
    
    
       @RequestMapping(value = "/ClaveEmergencia/ActualizarReclamoExtension/{v_eidreclamo}/{v_emontofactura}/{v_ecodigosuscripcion}/{v_ecodigotiposervicio}/{v_ecodigointervencion}/{v_ediagnostico}/{v_ecodigomedico}/{v_ecodigoclinica}/{v_estatus}/{v_econtactoclinica}/{v_eobservaciones}/{v_ecodareacelular}/{v_ecelular}/{v_efechaocurrencia}/{v_enumerocontrato}/{v_clasificacion}/{v_subclasificacion}", method = RequestMethod.GET, produces = "application/json")
    public String actualizareclamoextension(@PathVariable("v_eidreclamo") final String v_eidreclamo,
                                      @PathVariable("v_emontofactura") final String v_emontofactura,
                                      @PathVariable("v_ecodigosuscripcion") final String v_ecodigosuscripcion,
                                      @PathVariable("v_ecodigotiposervicio") final String v_ecodigotiposervicio,
                                      @PathVariable("v_ecodigointervencion") final String v_ecodigointervencion,
                                      @PathVariable("v_ediagnostico") final String v_ediagnostico,
                                      @PathVariable("v_ecodigomedico") final String v_ecodigomedico,
                                      @PathVariable("v_ecodigoclinica") final String v_ecodigoclinica,
                                      @PathVariable("v_estatus") final String v_estatus,
                                      @PathVariable("v_econtactoclinica") final String v_econtactoclinica,
                                      @PathVariable("v_eobservaciones") final String v_eobservaciones,
                                      @PathVariable("v_ecodareacelular") final String v_ecodareacelular,
                                      @PathVariable("v_ecelular") final String v_ecelular,
                                      @PathVariable("v_efechaocurrencia") final String v_efechaocurrencia ,
                                      @PathVariable("v_enumerocontrato") final String v_enumerocontrato ,
                                      @PathVariable("v_clasificacion") final String v_clasificacion ,
                                      @PathVariable("v_subclasificacion") final String v_subclasificacion 
                                         )
                                                                                        {
     return Web_ActualizaReclamoExtension.actualizareclamoextension(v_eidreclamo ,
                                                v_emontofactura ,
                                                v_ecodigosuscripcion,	 
                                                v_ecodigotiposervicio	 ,
                                                v_ecodigointervencion,	 
                                                v_ediagnostico,	 
                                                v_ecodigomedico ,
                                                v_ecodigoclinica	 ,
                                                v_estatus,
                                                v_econtactoclinica,
                                                v_eobservaciones,	 
                                                v_ecodareacelular,	 
                                                v_ecelular,
                                                v_efechaocurrencia,
                                                v_enumerocontrato,
                                                v_clasificacion,
                                                v_subclasificacion 
                                                 );
     } 
    
    
    
    
    
    
    
    
         
     @RequestMapping(value = "/Claves/ListadoClaves1/{v_clinica}/{v_fechaini}/{v_fechafin}/{v_status}", method = RequestMethod.GET, produces = "application/json")
    public String claveslistadoclaves2(@PathVariable("v_clinica") final String v_clinica,
                                      @PathVariable("v_fechaini") final String v_fechaini,
                                      @PathVariable("v_fechafin") final String v_fechafin, 
                                      @PathVariable("v_status") final String v_status) {
    
     
  
         
     return RepoClavesListadoClaves.claveslistadoclaves2(v_clinica,v_fechaini,v_fechafin,v_status);
    
                             
         }  

     //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/generarclave/genearaclavefinal/{v_parametro}", method = RequestMethod.GET, produces = "application/json")
    public String generarclavefinal(@PathVariable("v_parametro") final String v_parametro){
     return RepGenerarClaveFinal.generarclavefinal(v_parametro);
    }
    
    
 
    
      //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/generarclave/listadodiagnosticos/{v_contrato}/{v_suscripcion}", method = RequestMethod.GET, produces = "application/json")
    public String listadodiagnosticos(@PathVariable("v_contrato") final String v_contrato,
                                      @PathVariable("v_suscripcion") final String v_suscripcion) {
     return RepListadoDiagnosticos.listadodiagnosticos(v_contrato,v_suscripcion);
     } 
    
    //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/afiliado/listadoafiliados/{jcedula}/{jcontrato}", method = RequestMethod.GET, produces = "application/json")
    public String listadoafiliados(@PathVariable("jcedula") final String jcedula,
                                   @PathVariable("jcontrato") final String jcontrato) {
     return RepoListadoAfiliados.listadoafiliados(jcedula,jcontrato);
    }
    
    //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/afiliado/datoscontratantecobertura/{jcedula}", method = RequestMethod.GET, produces = "application/json")
    public String datoscontratantecobertura(@PathVariable("jcedula") final String jcedula){
     return RepoDatosContratanteCobertura.datoscontratantecobertura(jcedula);
    }
    
    
      //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/afiliado/validartitular/{jcedula}", method = RequestMethod.GET, produces = "application/json")
    public String validartitular(@PathVariable("jcedula") final String jcedula){
     return RepoValidarTitular.validartitular(jcedula);
    }
 
    //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/login/autenticarusuario/{login}/{clave}/{celular}", method = RequestMethod.GET, produces = "application/json")
    public String autenticarusuario(@PathVariable("login") final String login,
                                               @PathVariable("clave") final String clave,
                                               @PathVariable("celular") final String celular){
        return repoAutenticarUsuario.autenticarUsuario(login, clave, celular);
    }
    
    
    
    //@GetMapping(path = "/login/autenticarusuario/{login}/{clave}/{celular}")
    @RequestMapping(value = "/login/cambiarclave/{login}/{clave}/{celular}/{valida}", method = RequestMethod.GET, produces = "application/json")
    public String cambiarclave(@PathVariable("login") final String login,
                                               @PathVariable("clave") final String clave,
                                               @PathVariable("celular") final String celular,
                                                @PathVariable("valida") final String valida){
        return RepoCambiarClave.cambiarclave(login, clave, celular,valida);
    }

    /*@GetMapping(path = "/suscripciones/{certificado}")
    public List<Stp_pruebaRoberto> suscripcion(@PathVariable(value = "certificado") final String certificado){
        BigInteger cert = new BigInteger(certificado);
        return repoSpSuscripcion.obtieneSuscripciones(cert);
    }*/
    
    @PostMapping(path = "/agregarsuscripcion/{contrato}")
    public void agregarSuscripcion(@PathVariable(value = "contrato") final String contrato) throws Exception {
        Suscripcion sus;
        
        try {
            DbContextHolder.setDbType(DbType.Oracle);
            
            sus = repoSuscripcion.ultimaSuscripcion(contrato);
                        
            if (sus instanceof Suscripcion) {
                
                entityManager.detach(sus);
                
                sus.setCodigo(sus.getCodigo() + 1L);

                repoSuscripcion.save(sus);
            }
            else {
                throw new Exception("El número de contrato no existe");
            }
        }
        finally {
            DbContextHolder.clearDbType(); //Cierra conexion base de datos
        }
        
        
    }
}