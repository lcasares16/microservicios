/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo;

import java.io.Serializable;

/**
 *
 * @author admoracle
 */
public class RespuestaCursor implements Serializable {
    
    private String USERNAME;
    private String CELULAR_1;
    private String PASSWORD;
    private String NOMBRE_USUARIO;
    private String STATUS;
    
    public RespuestaCursor () {}

    /**
     * @return the USERNAME
     */
    public String getUSERNAME() {
        return USERNAME;
    }

    /**
     * @param USERNAME the USERNAME to set
     */
    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    /**
     * @return the CELULAR_1
     */
    public String getCELULAR_1() {
        return CELULAR_1;
    }

    /**
     * @param CELULAR_1 the CELULAR_1 to set
     */
    public void setCELULAR_1(String CELULAR_1) {
        this.CELULAR_1 = CELULAR_1;
    }

    /**
     * @return the PASSWORD
     */
    public String getPASSWORD() {
        return PASSWORD;
    }

    /**
     * @param PASSWORD the PASSWORD to set
     */
    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    /**
     * @return the NOMBRE_USUARIO
     */
    public String getNOMBRE_USUARIO() {
        return NOMBRE_USUARIO;
    }

    /**
     * @param NOMBRE_USUARIO the NOMBRE_USUARIO to set
     */
    public void setNOMBRE_USUARIO(String NOMBRE_USUARIO) {
        this.NOMBRE_USUARIO = NOMBRE_USUARIO;
    }

    /**
     * @return the STATUS
     */
    public String getSTATUS() {
        return STATUS;
    }

    /**
     * @param STATUS the STATUS to set
     */
    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
    
}
