/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.usuario.servicio;

import com.prov.evaluacion.modelo.Usuarios;

/**
 *
 * @author roberto.espinoza
 */
public interface InterfUsuario {
    
    public Usuarios buscarUsuario (String usuario, String clave);
    
}
