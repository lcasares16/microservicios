/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.usuario.impl;

import com.prov.evaluacion.modelo.Usuarios;
import com.prov.evaluacion.repositorio.usuario.RepoUsuario;
import com.prov.evaluacion.repositorio.usuario.servicio.InterfUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author roberto.espinoza
 */


@Service
@Transactional("OracletransactionManager")

public class ImplUsuario implements InterfUsuario {
    
    @Autowired
    private RepoUsuario repoUsuario;
    
    @Override
     public Usuarios buscarUsuario (String usuario, String clave) {
         return repoUsuario.find(usuario, clave);
     }
    
}
