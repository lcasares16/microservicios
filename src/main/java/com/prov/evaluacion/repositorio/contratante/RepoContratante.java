/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.contratante;

import com.prov.evaluacion.modelo.Contratante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author roberto.espinoza
 */

public interface RepoContratante extends JpaRepository<Contratante, Long>{
    
    Contratante findByNumeroContrato (@Param("numeroContrato") String numeroContrato);
    
    @Query("select c" +
           "  from Contratante c" +
           " where rowid = (select max(rowid)" +
           "                  from Contratante co)")
    Contratante ultimoContrato ();
    
       
}
