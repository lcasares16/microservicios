package com.prov.evaluacion;

import java.util.HashMap;
import java.util.Map;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class EvaluacionApplication {
    @Autowired
    private Environment env;
    
    @Bean
    RoutingDataSource dataSource() {

        RoutingDataSource routingDataSource = new RoutingDataSource();
        
        routingDataSource.setDefaultTargetDataSource(dataSourceOracle());

        Map<Object, Object> dbType = new HashMap<Object, Object>();

        dbType.put(DbType.Oracle, dataSourceOracle());

        routingDataSource.setTargetDataSources(dbType);
        routingDataSource.afterPropertiesSet();

        return routingDataSource;
    }
    
    BasicDataSource dataSourceOracle() {
 
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(env.getProperty("oracle.driver"));
        dataSource.setUsername(env.getProperty("oracle.username"));
        dataSource.setPassword(env.getProperty("oracle.password"));
        dataSource.setUrl(env.getProperty("oracle.url"));
        return dataSource;
    }
	public static void main(String[] args) {
		SpringApplication.run(EvaluacionApplication.class, args);
	}
}
