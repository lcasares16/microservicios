/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
@Repository

public class Web_GrabarRegistrosDocumentos {
   @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    
    
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PCK_WEB_ClaveEmergencia")
                .withProcedureName("GrabarRegistrosDocumentos")
                .declareParameters(
                        new SqlParameter("v_tipo_documento", Types.INTEGER),
                        new SqlParameter("v_tipo_servicio", Types.INTEGER),
                        new SqlParameter("v_plataforma", Types.INTEGER),
                        new SqlParameter("v_forma", Types.VARCHAR),
                        new SqlParameter("v_numero_contrato", Types.VARCHAR),
                        new SqlParameter("v_certificado", Types.VARCHAR),
                        new SqlParameter("v_cedula", Types.VARCHAR),
                        new SqlParameter("v_codigo_suscripcion", Types.INTEGER),
                        new SqlParameter("v_id_reclamo", Types.INTEGER),
                        new SqlParameter("v_nombre_documento", Types.VARCHAR),
                                            
                    new SqlOutParameter("mensaje11", Types.VARCHAR)
                );
    
}
    public String grabarregistrosdocumentos (String j_tipo_documento,String j_tipo_servicio, String j_plataforma ,
                                   String j_forma, String j_numero_contrato, String j_certificado,String j_cedula,	 
                                   String j_codigo_suscripcion, String j_id_reclamo,String j_nombre_documento)
    
    {
       
       // (String jcedula,String jcontrato) {
        
      // map<Integer, object> result=  simpleJdbcCall.execute(jcedula.trim());
       Map<String, Object> result = simpleJdbcCall.execute(j_tipo_documento.trim(),j_tipo_servicio.trim(), j_plataforma.trim() ,
                                                            j_forma.trim(), j_numero_contrato.trim(), j_certificado.trim(),j_cedula.trim(),	 
                                                            j_codigo_suscripcion.trim(), j_id_reclamo.trim(),j_nombre_documento.trim() );
       String resultado = (String) result.get("mensaje11");
       return resultado;
    }   
}
