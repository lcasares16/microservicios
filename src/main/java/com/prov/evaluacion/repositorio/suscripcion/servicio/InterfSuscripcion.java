/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.servicio;

import com.prov.evaluacion.modelo.Suscripcion;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author roberto.espinoza
 */
public interface InterfSuscripcion {
    
    public Suscripcion ultimaSuscripcion(@Param("numeroContrato") final String numeroContrato);
    
}
