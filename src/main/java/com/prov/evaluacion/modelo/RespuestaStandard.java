/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo;

import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author admoracle
 */
public class RespuestaStandard  implements Serializable {

    private String MENS1;
    private String MENS2;
    private String V_FECHA;
    private String C_AUTENTICARUSUARIO;
    private Collection<RespuestaCursor> respuestaCursor;
    
    public RespuestaStandard () {}

    /**
     * @return the MENS1
     */
    public String getMENS1() {
        return MENS1;
    }

    /**
     * @param MENS1 the MENS1 to set
     */
    public void setMENS1(String MENS1) {
        this.MENS1 = MENS1;
    }

    /**
     * @return the MENS2
     */
    public String getMENS2() {
        return MENS2;
    }

    /**
     * @param MENS2 the MENS2 to set
     */
    public void setMENS2(String MENS2) {
        this.MENS2 = MENS2;
    }

    /**
     * @return the V_FECHA
     */
    public String getV_FECHA() {
        return V_FECHA;
    }

    /**
     * @param V_FECHA the V_FECHA to set
     */
    public void setV_FECHA(String V_FECHA) {
        this.V_FECHA = V_FECHA;
    }

    /**
     * @return the C_AUTENTICARUSUARIO
     */
    public String getC_AUTENTICARUSUARIO() {
        return C_AUTENTICARUSUARIO;
    }

    /**
     * @param C_AUTENTICARUSUARIO the C_AUTENTICARUSUARIO to set
     */
    public void setC_AUTENTICARUSUARIO(String C_AUTENTICARUSUARIO) {
        this.C_AUTENTICARUSUARIO = C_AUTENTICARUSUARIO;
    }

    /**
     * @return the respuestaCursor
     */
    public Collection<RespuestaCursor> getRespuestaCursor() {
        return respuestaCursor;
    }

    /**
     * @param respuestaCursor the respuestaCursor to set
     */
    public void setRespuestaCursor(Collection<RespuestaCursor> respuestaCursor) {
        this.respuestaCursor = respuestaCursor;
    }
    
   
    
}
