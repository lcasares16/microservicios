/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.impl;

import com.prov.evaluacion.modelo.Suscripcion;
import com.prov.evaluacion.repositorio.suscripcion.RepoSuscripcion;
import com.prov.evaluacion.repositorio.suscripcion.servicio.InterfSuscripcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author roberto.espinoza
 */

@Service
@Transactional("OracletransactionManager")

public class ImplSuscripcion implements InterfSuscripcion {
    
    @Autowired
    private RepoSuscripcion repoSuscripcion;
    
    @Override
    public Suscripcion ultimaSuscripcion(@Param("numeroContrato") final String numeroContrato) {
        return repoSuscripcion.ultimaSuscripcion(numeroContrato);
    }
}
