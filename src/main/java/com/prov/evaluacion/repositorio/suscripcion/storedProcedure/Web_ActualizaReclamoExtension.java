/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
@Repository


public class Web_ActualizaReclamoExtension {
     @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    
    
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PCK_WEB_ClaveEmergencia")
                .withProcedureName("ActualizaReclamoExtension")
                .declareParameters(
                    new SqlParameter("V_eIdReclamo", Types.INTEGER),
                    new SqlParameter("V_eMontoFactura", Types.VARCHAR),  
                    new SqlParameter("V_eCodigoSuscripcion", Types.VARCHAR),  
                    new SqlParameter("V_eCodigoTipoServicio", Types.VARCHAR),  
                    new SqlParameter("V_eCodigoIntervencion", Types.VARCHAR), 
                    new SqlParameter("V_eDiagnostico", Types.VARCHAR),
                    new SqlParameter("V_eCodigoMedico", Types.VARCHAR),  
                    new SqlParameter("V_eCodigoClinica", Types.VARCHAR),  
                    new SqlParameter("V_eStatus", Types.VARCHAR),  
                    new SqlParameter("V_eContactoClinica", Types.VARCHAR),  
                    new SqlParameter("V_eObservaciones", Types.VARCHAR),  
                    new SqlParameter("V_eCodAreaCelular", Types.VARCHAR),  
                    new SqlParameter("V_eCelular", Types.VARCHAR), 
                    new SqlParameter("V_eFechaOcurrencia", Types.VARCHAR), 
                    new SqlParameter("V_eNumeroContrato", Types.VARCHAR), 
                    new SqlParameter("V_clasificacion", Types.VARCHAR), 
                    new SqlParameter("V_subclasificacion", Types.VARCHAR), 
                    
                    new SqlOutParameter("mensaje11", Types.VARCHAR)
     
                             
                );
}
    public String actualizareclamoextension(String V_eIdReclamo,
                                      String V_eMontoFactura,
                                      String V_eCodigoSuscripcion,
                                      String v_codigo_tipo_servicio,
                                      String V_eCodigoIntervencion,
                                      String V_eDiagnostico,
                                      String V_eCodigoMedico,
                                      String V_eCodigoClinica,
                                      String V_eStatus,
                                      String V_eContactoClinica,
                                      String V_eObservaciones,
                                      String V_eCodAreaCelular,
                                      String V_eCelular,
                                      String V_eFechaOcurrencia,
                                      String V_eNumeroContrato,
                                      String v_clasificacion,
                                      String v_subclasificacion
    ) {
            
      // map<Integer, object> result=  simpleJdbcCall.execute(jcedula.trim());
       Map<String, Object> result = simpleJdbcCall.execute(V_eIdReclamo.trim(),
                                                           V_eMontoFactura.trim(),
                                                           V_eCodigoSuscripcion.trim(),
                                                           v_codigo_tipo_servicio.trim(),
                                                           V_eCodigoIntervencion.trim(),
                                                           V_eDiagnostico.trim(),
                                                           V_eCodigoMedico.trim(),
                                                           V_eCodigoClinica.trim(),
                                                           V_eStatus.trim(),
                                                           V_eContactoClinica.trim(),
                                                           V_eObservaciones.trim(),
                                                           V_eCodAreaCelular.trim(),
                                                           V_eCelular.trim(),
                                                           V_eFechaOcurrencia.trim(),
                                                           V_eNumeroContrato.trim(),
                                                           v_clasificacion.trim(),
                                                           v_subclasificacion.trim()
               
                                                            );
                                                          
       String resultado = (String) result.get("mensaje11");
       return resultado;
    }  
}
