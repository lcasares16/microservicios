/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author roberto.espinoza
 */
@Entity
@Table(name = "CONTRATANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contratante.findAll", query = "SELECT c FROM Contratante c")
    , @NamedQuery(name = "Contratante.findByNumeroContrato", query = "SELECT c FROM Contratante c WHERE c.numeroContrato = :numeroContrato")
    , @NamedQuery(name = "Contratante.findByNombre", query = "SELECT c FROM Contratante c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "Contratante.findByRif", query = "SELECT c FROM Contratante c WHERE c.rif = :rif")
    , @NamedQuery(name = "Contratante.findByFechaCreacion", query = "SELECT c FROM Contratante c WHERE c.fechaCreacion = :fechaCreacion")
    , @NamedQuery(name = "Contratante.findByResponsable", query = "SELECT c FROM Contratante c WHERE c.responsable = :responsable")
    , @NamedQuery(name = "Contratante.findByContacto", query = "SELECT c FROM Contratante c WHERE c.contacto = :contacto")
    , @NamedQuery(name = "Contratante.findByNit", query = "SELECT c FROM Contratante c WHERE c.nit = :nit")
    , @NamedQuery(name = "Contratante.findByAgente", query = "SELECT c FROM Contratante c WHERE c.agente = :agente")
    , @NamedQuery(name = "Contratante.findByObservacionAgente", query = "SELECT c FROM Contratante c WHERE c.observacionAgente = :observacionAgente")
    , @NamedQuery(name = "Contratante.findByDireccion", query = "SELECT c FROM Contratante c WHERE c.direccion = :direccion")
    , @NamedQuery(name = "Contratante.findByObservacion", query = "SELECT c FROM Contratante c WHERE c.observacion = :observacion")
    , @NamedQuery(name = "Contratante.findByFechaDesde", query = "SELECT c FROM Contratante c WHERE c.fechaDesde = :fechaDesde")
    , @NamedQuery(name = "Contratante.findByFechaHasta", query = "SELECT c FROM Contratante c WHERE c.fechaHasta = :fechaHasta")
    , @NamedQuery(name = "Contratante.findByFechaDesdeExceso", query = "SELECT c FROM Contratante c WHERE c.fechaDesdeExceso = :fechaDesdeExceso")
    , @NamedQuery(name = "Contratante.findByFechaHastaExceso", query = "SELECT c FROM Contratante c WHERE c.fechaHastaExceso = :fechaHastaExceso")
    , @NamedQuery(name = "Contratante.findByStatus", query = "SELECT c FROM Contratante c WHERE c.status = :status")
    , @NamedQuery(name = "Contratante.findByFechaAnulacion", query = "SELECT c FROM Contratante c WHERE c.fechaAnulacion = :fechaAnulacion")
    , @NamedQuery(name = "Contratante.findByPorcentajeComision", query = "SELECT c FROM Contratante c WHERE c.porcentajeComision = :porcentajeComision")
    , @NamedQuery(name = "Contratante.findByComisionQualitas", query = "SELECT c FROM Contratante c WHERE c.comisionQualitas = :comisionQualitas")
    , @NamedQuery(name = "Contratante.findByComisionAgente", query = "SELECT c FROM Contratante c WHERE c.comisionAgente = :comisionAgente")
    , @NamedQuery(name = "Contratante.findByObservacionesQualitas", query = "SELECT c FROM Contratante c WHERE c.observacionesQualitas = :observacionesQualitas")
    , @NamedQuery(name = "Contratante.findByMontoRestitucion", query = "SELECT c FROM Contratante c WHERE c.montoRestitucion = :montoRestitucion")
    , @NamedQuery(name = "Contratante.findByExentoIva", query = "SELECT c FROM Contratante c WHERE c.exentoIva = :exentoIva")
    , @NamedQuery(name = "Contratante.findByContEspecial", query = "SELECT c FROM Contratante c WHERE c.contEspecial = :contEspecial")
    , @NamedQuery(name = "Contratante.findBySucursal", query = "SELECT c FROM Contratante c WHERE c.sucursal = :sucursal")
    , @NamedQuery(name = "Contratante.findByFormaPago", query = "SELECT c FROM Contratante c WHERE c.formaPago = :formaPago")
    , @NamedQuery(name = "Contratante.findByTipo", query = "SELECT c FROM Contratante c WHERE c.tipo = :tipo")
    , @NamedQuery(name = "Contratante.findByVigenciaDesde", query = "SELECT c FROM Contratante c WHERE c.vigenciaDesde = :vigenciaDesde")
    , @NamedQuery(name = "Contratante.findByVigenciaHasta", query = "SELECT c FROM Contratante c WHERE c.vigenciaHasta = :vigenciaHasta")
    , @NamedQuery(name = "Contratante.findByCodigoSucursal", query = "SELECT c FROM Contratante c WHERE c.codigoSucursal = :codigoSucursal")
    , @NamedQuery(name = "Contratante.findByCodRamo", query = "SELECT c FROM Contratante c WHERE c.codRamo = :codRamo")
    , @NamedQuery(name = "Contratante.findByCodSubRamo", query = "SELECT c FROM Contratante c WHERE c.codSubRamo = :codSubRamo")
    , @NamedQuery(name = "Contratante.findByModificadoPor", query = "SELECT c FROM Contratante c WHERE c.modificadoPor = :modificadoPor")
    , @NamedQuery(name = "Contratante.findByFechaModificacion", query = "SELECT c FROM Contratante c WHERE c.fechaModificacion = :fechaModificacion")
    , @NamedQuery(name = "Contratante.findByFechaModif", query = "SELECT c FROM Contratante c WHERE c.fechaModif = :fechaModif")
    , @NamedQuery(name = "Contratante.findByMaquina", query = "SELECT c FROM Contratante c WHERE c.maquina = :maquina")
    , @NamedQuery(name = "Contratante.findByHechoPor", query = "SELECT c FROM Contratante c WHERE c.hechoPor = :hechoPor")
    , @NamedQuery(name = "Contratante.findByOrigen", query = "SELECT c FROM Contratante c WHERE c.origen = :origen")
    , @NamedQuery(name = "Contratante.findByBeneficiario", query = "SELECT c FROM Contratante c WHERE c.beneficiario = :beneficiario")
    , @NamedQuery(name = "Contratante.findByMatrix", query = "SELECT c FROM Contratante c WHERE c.matrix = :matrix")
    , @NamedQuery(name = "Contratante.findByAuxiliar", query = "SELECT c FROM Contratante c WHERE c.auxiliar = :auxiliar")
    , @NamedQuery(name = "Contratante.findByCodLoc", query = "SELECT c FROM Contratante c WHERE c.codLoc = :codLoc")
    , @NamedQuery(name = "Contratante.findByEstadoCuenta", query = "SELECT c FROM Contratante c WHERE c.estadoCuenta = :estadoCuenta")
    , @NamedQuery(name = "Contratante.findByAsistencia", query = "SELECT c FROM Contratante c WHERE c.asistencia = :asistencia")
    , @NamedQuery(name = "Contratante.findByAmbulancia", query = "SELECT c FROM Contratante c WHERE c.ambulancia = :ambulancia")
    , @NamedQuery(name = "Contratante.findByFechaAsistencia", query = "SELECT c FROM Contratante c WHERE c.fechaAsistencia = :fechaAsistencia")
    , @NamedQuery(name = "Contratante.findByFechaAmbulancia", query = "SELECT c FROM Contratante c WHERE c.fechaAmbulancia = :fechaAmbulancia")
    , @NamedQuery(name = "Contratante.findByFechaRegistro", query = "SELECT c FROM Contratante c WHERE c.fechaRegistro = :fechaRegistro")
    , @NamedQuery(name = "Contratante.findByAtencion", query = "SELECT c FROM Contratante c WHERE c.atencion = :atencion")
    , @NamedQuery(name = "Contratante.findByQualimed", query = "SELECT c FROM Contratante c WHERE c.qualimed = :qualimed")
    , @NamedQuery(name = "Contratante.findByFechaAtencion", query = "SELECT c FROM Contratante c WHERE c.fechaAtencion = :fechaAtencion")
    , @NamedQuery(name = "Contratante.findByFechaQualimed", query = "SELECT c FROM Contratante c WHERE c.fechaQualimed = :fechaQualimed")
    , @NamedQuery(name = "Contratante.findByTipoQualimed", query = "SELECT c FROM Contratante c WHERE c.tipoQualimed = :tipoQualimed")
    , @NamedQuery(name = "Contratante.findByCantidadQualimed", query = "SELECT c FROM Contratante c WHERE c.cantidadQualimed = :cantidadQualimed")
    , @NamedQuery(name = "Contratante.findByMasivo", query = "SELECT c FROM Contratante c WHERE c.masivo = :masivo")
    , @NamedQuery(name = "Contratante.findByDigitalizacion", query = "SELECT c FROM Contratante c WHERE c.digitalizacion = :digitalizacion")
    , @NamedQuery(name = "Contratante.findByPolizaFronti", query = "SELECT c FROM Contratante c WHERE c.polizaFronti = :polizaFronti")
    , @NamedQuery(name = "Contratante.findByNombreWeb", query = "SELECT c FROM Contratante c WHERE c.nombreWeb = :nombreWeb")
    , @NamedQuery(name = "Contratante.findByTipoProductoWeb", query = "SELECT c FROM Contratante c WHERE c.tipoProductoWeb = :tipoProductoWeb")
    , @NamedQuery(name = "Contratante.findByCronico", query = "SELECT c FROM Contratante c WHERE c.cronico = :cronico")
    , @NamedQuery(name = "Contratante.findByMilRazones", query = "SELECT c FROM Contratante c WHERE c.milRazones = :milRazones")
    , @NamedQuery(name = "Contratante.findByComisionQualimed", query = "SELECT c FROM Contratante c WHERE c.comisionQualimed = :comisionQualimed")
    , @NamedQuery(name = "Contratante.findByTipoCobranza", query = "SELECT c FROM Contratante c WHERE c.tipoCobranza = :tipoCobranza")
    , @NamedQuery(name = "Contratante.findByCodigoSucursalOperativa", query = "SELECT c FROM Contratante c WHERE c.codigoSucursalOperativa = :codigoSucursalOperativa")
    , @NamedQuery(name = "Contratante.findByHumaTicket", query = "SELECT c FROM Contratante c WHERE c.humaTicket = :humaTicket")
    , @NamedQuery(name = "Contratante.findByCodFact", query = "SELECT c FROM Contratante c WHERE c.codFact = :codFact")
    , @NamedQuery(name = "Contratante.findByProveedorAmd", query = "SELECT c FROM Contratante c WHERE c.proveedorAmd = :proveedorAmd")
    , @NamedQuery(name = "Contratante.findByFacturacionGlobal", query = "SELECT c FROM Contratante c WHERE c.facturacionGlobal = :facturacionGlobal")
    , @NamedQuery(name = "Contratante.findByOrdenCompra", query = "SELECT c FROM Contratante c WHERE c.ordenCompra = :ordenCompra")
    , @NamedQuery(name = "Contratante.findByFechaTopeSntro", query = "SELECT c FROM Contratante c WHERE c.fechaTopeSntro = :fechaTopeSntro")
    , @NamedQuery(name = "Contratante.findByFacturaNombreDe", query = "SELECT c FROM Contratante c WHERE c.facturaNombreDe = :facturaNombreDe")
    , @NamedQuery(name = "Contratante.findByRifFactura", query = "SELECT c FROM Contratante c WHERE c.rifFactura = :rifFactura")
    , @NamedQuery(name = "Contratante.findByDireccionFiscalFact", query = "SELECT c FROM Contratante c WHERE c.direccionFiscalFact = :direccionFiscalFact")
    , @NamedQuery(name = "Contratante.findByTelfFactura", query = "SELECT c FROM Contratante c WHERE c.telfFactura = :telfFactura")})
public class Contratante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    ////@NotNull
    //@Size(min = 1, max = 10)
    @Column(name = "NUMERO_CONTRATO")
    private String numeroContrato;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 250)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 15)
    @Column(name = "RIF")
    private String rif;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "FECHA_CREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 50)
    @Column(name = "RESPONSABLE")
    private String responsable;
    //@Size(max = 50)
    @Column(name = "CONTACTO")
    private String contacto;
    //@Size(max = 15)
    @Column(name = "NIT")
    private String nit;
    //@Size(max = 50)
    @Column(name = "AGENTE")
    private String agente;
    //@Size(max = 2000)
    @Column(name = "OBSERVACION_AGENTE")
    private String observacionAgente;
    //@Size(max = 2000)
    @Column(name = "DIRECCION")
    private String direccion;
    //@Size(max = 2000)
    @Column(name = "OBSERVACION")
    private String observacion;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "FECHA_DESDE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDesde;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "FECHA_HASTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHasta;
    @Column(name = "FECHA_DESDE_EXCESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaDesdeExceso;
    @Column(name = "FECHA_HASTA_EXCESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHastaExceso;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 10)
    @Column(name = "STATUS")
    private String status;
    @Column(name = "FECHA_ANULACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAnulacion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PORCENTAJE_COMISION")
    private BigDecimal porcentajeComision;
    @Column(name = "COMISION_QUALITAS")
    private BigDecimal comisionQualitas;
    @Column(name = "COMISION_AGENTE")
    private BigDecimal comisionAgente;
    //@Size(max = 2000)
    @Column(name = "OBSERVACIONES_QUALITAS")
    private String observacionesQualitas;
    @Column(name = "MONTO_RESTITUCION")
    private BigDecimal montoRestitucion;
    //@Size(max = 2)
    @Column(name = "EXENTO_IVA")
    private String exentoIva;
    //@Size(max = 2)
    @Column(name = "CONT_ESPECIAL")
    private String contEspecial;
    //@Size(max = 20)
    @Column(name = "SUCURSAL")
    private String sucursal;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 20)
    @Column(name = "FORMA_PAGO")
    private String formaPago;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 12)
    @Column(name = "TIPO")
    private String tipo;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "VIGENCIA_DESDE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vigenciaDesde;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "VIGENCIA_HASTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vigenciaHasta;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "CODIGO_SUCURSAL")
    private short codigoSucursal;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 4)
    @Column(name = "COD_RAMO")
    private String codRamo;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 4)
    @Column(name = "COD_SUB_RAMO")
    private String codSubRamo;
    //@Size(max = 30)
    @Column(name = "MODIFICADO_POR")
    private String modificadoPor;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    //@Size(max = 20)
    @Column(name = "FECHA_MODIF")
    private String fechaModif;
    //@Size(max = 30)
    @Column(name = "MAQUINA")
    private String maquina;
    //@Size(max = 30)
    @Column(name = "HECHO_POR")
    private String hechoPor;
    //@Size(max = 20)
    @Column(name = "ORIGEN")
    private String origen;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 2)
    @Column(name = "BENEFICIARIO")
    private String beneficiario;
    //@Size(max = 10)
    @Column(name = "MATRIX")
    private String matrix;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "AUXILIAR")
    private int auxiliar;
    //@Size(max = 10)
    @Column(name = "COD_LOC")
    private String codLoc;
    //@Size(max = 2)
    @Column(name = "ESTADO_CUENTA")
    private String estadoCuenta;
    //@Size(max = 2)
    @Column(name = "ASISTENCIA")
    private String asistencia;
    //@Size(max = 2)
    @Column(name = "AMBULANCIA")
    private String ambulancia;
    @Column(name = "FECHA_ASISTENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAsistencia;
    @Column(name = "FECHA_AMBULANCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAmbulancia;
    @Column(name = "FECHA_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    //@Size(max = 2)
    @Column(name = "ATENCION")
    private String atencion;
    //@Size(max = 2)
    @Column(name = "QUALIMED")
    private String qualimed;
    @Column(name = "FECHA_ATENCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAtencion;
    @Column(name = "FECHA_QUALIMED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaQualimed;
    //@Size(max = 20)
    @Column(name = "TIPO_QUALIMED")
    private String tipoQualimed;
    @Column(name = "CANTIDAD_QUALIMED")
    private Long cantidadQualimed;
    //@Size(max = 2)
    @Column(name = "MASIVO")
    private String masivo;
    //@Size(max = 100)
    @Column(name = "DIGITALIZACION")
    private String digitalizacion;
    //@Size(max = 50)
    @Column(name = "POLIZA_FRONTI")
    private String polizaFronti;
    //@Size(max = 250)
    @Column(name = "NOMBRE_WEB")
    private String nombreWeb;
    //@Size(max = 20)
    @Column(name = "TIPO_PRODUCTO_WEB")
    private String tipoProductoWeb;
    //@Size(max = 2)
    @Column(name = "CRONICO")
    private String cronico;
    //@Size(max = 2)
    @Column(name = "MIL_RAZONES")
    private String milRazones;
    @Column(name = "COMISION_QUALIMED")
    private BigDecimal comisionQualimed;
    //@Size(max = 50)
    @Column(name = "TIPO_COBRANZA")
    private String tipoCobranza;
    @Column(name = "CODIGO_SUCURSAL_OPERATIVA")
    private Short codigoSucursalOperativa;
    //@Size(max = 2)
    @Column(name = "HUMA_TICKET")
    private String humaTicket;
    //@Size(max = 12)
    @Column(name = "COD_FACT")
    private String codFact;
    //@Size(max = 50)
    @Column(name = "PROVEEDOR_AMD")
    private String proveedorAmd;
    //@Size(max = 2)
    @Column(name = "FACTURACION_GLOBAL")
    private String facturacionGlobal;
    //@Size(max = 2)
    @Column(name = "ORDEN_COMPRA")
    private String ordenCompra;
    @Column(name = "FECHA_TOPE_SNTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaTopeSntro;
    //@Size(max = 1000)
    @Column(name = "FACTURA_NOMBRE_DE")
    private String facturaNombreDe;
    //@Size(max = 20)
    @Column(name = "RIF_FACTURA")
    private String rifFactura;
    //@Size(max = 2000)
    @Column(name = "DIRECCION_FISCAL_FACT")
    private String direccionFiscalFact;
    //@Size(max = 20)
    @Column(name = "TELF_FACTURA")
    private String telfFactura;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "numeroContrato")
    private Collection<Suscripcion> suscripcionCollection;

    public Contratante() {
    }

    public Contratante(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public Contratante(String numeroContrato, String nombre, String rif, Date fechaCreacion, String responsable, Date fechaDesde, Date fechaHasta, String status, String formaPago, String tipo, Date vigenciaDesde, Date vigenciaHasta, short codigoSucursal, String codRamo, String codSubRamo, String beneficiario, int auxiliar) {
        this.numeroContrato = numeroContrato;
        this.nombre = nombre;
        this.rif = rif;
        this.fechaCreacion = fechaCreacion;
        this.responsable = responsable;
        this.fechaDesde = fechaDesde;
        this.fechaHasta = fechaHasta;
        this.status = status;
        this.formaPago = formaPago;
        this.tipo = tipo;
        this.vigenciaDesde = vigenciaDesde;
        this.vigenciaHasta = vigenciaHasta;
        this.codigoSucursal = codigoSucursal;
        this.codRamo = codRamo;
        this.codSubRamo = codSubRamo;
        this.beneficiario = beneficiario;
        this.auxiliar = auxiliar;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getAgente() {
        return agente;
    }

    public void setAgente(String agente) {
        this.agente = agente;
    }

    public String getObservacionAgente() {
        return observacionAgente;
    }

    public void setObservacionAgente(String observacionAgente) {
        this.observacionAgente = observacionAgente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Date fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Date getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Date fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Date getFechaDesdeExceso() {
        return fechaDesdeExceso;
    }

    public void setFechaDesdeExceso(Date fechaDesdeExceso) {
        this.fechaDesdeExceso = fechaDesdeExceso;
    }

    public Date getFechaHastaExceso() {
        return fechaHastaExceso;
    }

    public void setFechaHastaExceso(Date fechaHastaExceso) {
        this.fechaHastaExceso = fechaHastaExceso;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaAnulacion() {
        return fechaAnulacion;
    }

    public void setFechaAnulacion(Date fechaAnulacion) {
        this.fechaAnulacion = fechaAnulacion;
    }

    public BigDecimal getPorcentajeComision() {
        return porcentajeComision;
    }

    public void setPorcentajeComision(BigDecimal porcentajeComision) {
        this.porcentajeComision = porcentajeComision;
    }

    public BigDecimal getComisionQualitas() {
        return comisionQualitas;
    }

    public void setComisionQualitas(BigDecimal comisionQualitas) {
        this.comisionQualitas = comisionQualitas;
    }

    public BigDecimal getComisionAgente() {
        return comisionAgente;
    }

    public void setComisionAgente(BigDecimal comisionAgente) {
        this.comisionAgente = comisionAgente;
    }

    public String getObservacionesQualitas() {
        return observacionesQualitas;
    }

    public void setObservacionesQualitas(String observacionesQualitas) {
        this.observacionesQualitas = observacionesQualitas;
    }

    public BigDecimal getMontoRestitucion() {
        return montoRestitucion;
    }

    public void setMontoRestitucion(BigDecimal montoRestitucion) {
        this.montoRestitucion = montoRestitucion;
    }

    public String getExentoIva() {
        return exentoIva;
    }

    public void setExentoIva(String exentoIva) {
        this.exentoIva = exentoIva;
    }

    public String getContEspecial() {
        return contEspecial;
    }

    public void setContEspecial(String contEspecial) {
        this.contEspecial = contEspecial;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(Date vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public Date getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(Date vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public short getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(short codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public String getCodRamo() {
        return codRamo;
    }

    public void setCodRamo(String codRamo) {
        this.codRamo = codRamo;
    }

    public String getCodSubRamo() {
        return codSubRamo;
    }

    public void setCodSubRamo(String codSubRamo) {
        this.codSubRamo = codSubRamo;
    }

    public String getModificadoPor() {
        return modificadoPor;
    }

    public void setModificadoPor(String modificadoPor) {
        this.modificadoPor = modificadoPor;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getFechaModif() {
        return fechaModif;
    }

    public void setFechaModif(String fechaModif) {
        this.fechaModif = fechaModif;
    }

    public String getMaquina() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina = maquina;
    }

    public String getHechoPor() {
        return hechoPor;
    }

    public void setHechoPor(String hechoPor) {
        this.hechoPor = hechoPor;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(String beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getMatrix() {
        return matrix;
    }

    public void setMatrix(String matrix) {
        this.matrix = matrix;
    }

    public int getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(int auxiliar) {
        this.auxiliar = auxiliar;
    }

    public String getCodLoc() {
        return codLoc;
    }

    public void setCodLoc(String codLoc) {
        this.codLoc = codLoc;
    }

    public String getEstadoCuenta() {
        return estadoCuenta;
    }

    public void setEstadoCuenta(String estadoCuenta) {
        this.estadoCuenta = estadoCuenta;
    }

    public String getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String asistencia) {
        this.asistencia = asistencia;
    }

    public String getAmbulancia() {
        return ambulancia;
    }

    public void setAmbulancia(String ambulancia) {
        this.ambulancia = ambulancia;
    }

    public Date getFechaAsistencia() {
        return fechaAsistencia;
    }

    public void setFechaAsistencia(Date fechaAsistencia) {
        this.fechaAsistencia = fechaAsistencia;
    }

    public Date getFechaAmbulancia() {
        return fechaAmbulancia;
    }

    public void setFechaAmbulancia(Date fechaAmbulancia) {
        this.fechaAmbulancia = fechaAmbulancia;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getAtencion() {
        return atencion;
    }

    public void setAtencion(String atencion) {
        this.atencion = atencion;
    }

    public String getQualimed() {
        return qualimed;
    }

    public void setQualimed(String qualimed) {
        this.qualimed = qualimed;
    }

    public Date getFechaAtencion() {
        return fechaAtencion;
    }

    public void setFechaAtencion(Date fechaAtencion) {
        this.fechaAtencion = fechaAtencion;
    }

    public Date getFechaQualimed() {
        return fechaQualimed;
    }

    public void setFechaQualimed(Date fechaQualimed) {
        this.fechaQualimed = fechaQualimed;
    }

    public String getTipoQualimed() {
        return tipoQualimed;
    }

    public void setTipoQualimed(String tipoQualimed) {
        this.tipoQualimed = tipoQualimed;
    }

    public Long getCantidadQualimed() {
        return cantidadQualimed;
    }

    public void setCantidadQualimed(Long cantidadQualimed) {
        this.cantidadQualimed = cantidadQualimed;
    }

    public String getMasivo() {
        return masivo;
    }

    public void setMasivo(String masivo) {
        this.masivo = masivo;
    }

    public String getDigitalizacion() {
        return digitalizacion;
    }

    public void setDigitalizacion(String digitalizacion) {
        this.digitalizacion = digitalizacion;
    }

    public String getPolizaFronti() {
        return polizaFronti;
    }

    public void setPolizaFronti(String polizaFronti) {
        this.polizaFronti = polizaFronti;
    }

    public String getNombreWeb() {
        return nombreWeb;
    }

    public void setNombreWeb(String nombreWeb) {
        this.nombreWeb = nombreWeb;
    }

    public String getTipoProductoWeb() {
        return tipoProductoWeb;
    }

    public void setTipoProductoWeb(String tipoProductoWeb) {
        this.tipoProductoWeb = tipoProductoWeb;
    }

    public String getCronico() {
        return cronico;
    }

    public void setCronico(String cronico) {
        this.cronico = cronico;
    }

    public String getMilRazones() {
        return milRazones;
    }

    public void setMilRazones(String milRazones) {
        this.milRazones = milRazones;
    }

    public BigDecimal getComisionQualimed() {
        return comisionQualimed;
    }

    public void setComisionQualimed(BigDecimal comisionQualimed) {
        this.comisionQualimed = comisionQualimed;
    }

    public String getTipoCobranza() {
        return tipoCobranza;
    }

    public void setTipoCobranza(String tipoCobranza) {
        this.tipoCobranza = tipoCobranza;
    }

    public Short getCodigoSucursalOperativa() {
        return codigoSucursalOperativa;
    }

    public void setCodigoSucursalOperativa(Short codigoSucursalOperativa) {
        this.codigoSucursalOperativa = codigoSucursalOperativa;
    }

    public String getHumaTicket() {
        return humaTicket;
    }

    public void setHumaTicket(String humaTicket) {
        this.humaTicket = humaTicket;
    }

    public String getCodFact() {
        return codFact;
    }

    public void setCodFact(String codFact) {
        this.codFact = codFact;
    }

    public String getProveedorAmd() {
        return proveedorAmd;
    }

    public void setProveedorAmd(String proveedorAmd) {
        this.proveedorAmd = proveedorAmd;
    }

    public String getFacturacionGlobal() {
        return facturacionGlobal;
    }

    public void setFacturacionGlobal(String facturacionGlobal) {
        this.facturacionGlobal = facturacionGlobal;
    }

    public String getOrdenCompra() {
        return ordenCompra;
    }

    public void setOrdenCompra(String ordenCompra) {
        this.ordenCompra = ordenCompra;
    }

    public Date getFechaTopeSntro() {
        return fechaTopeSntro;
    }

    public void setFechaTopeSntro(Date fechaTopeSntro) {
        this.fechaTopeSntro = fechaTopeSntro;
    }

    public String getFacturaNombreDe() {
        return facturaNombreDe;
    }

    public void setFacturaNombreDe(String facturaNombreDe) {
        this.facturaNombreDe = facturaNombreDe;
    }

    public String getRifFactura() {
        return rifFactura;
    }

    public void setRifFactura(String rifFactura) {
        this.rifFactura = rifFactura;
    }

    public String getDireccionFiscalFact() {
        return direccionFiscalFact;
    }

    public void setDireccionFiscalFact(String direccionFiscalFact) {
        this.direccionFiscalFact = direccionFiscalFact;
    }

    public String getTelfFactura() {
        return telfFactura;
    }

    public void setTelfFactura(String telfFactura) {
        this.telfFactura = telfFactura;
    }

    @XmlTransient
    public Collection<Suscripcion> getSuscripcionCollection() {
        return suscripcionCollection;
    }

    public void setSuscripcionCollection(Collection<Suscripcion> suscripcionCollection) {
        this.suscripcionCollection = suscripcionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numeroContrato != null ? numeroContrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contratante)) {
            return false;
        }
        Contratante other = (Contratante) object;
        if ((this.numeroContrato == null && other.numeroContrato != null) || (this.numeroContrato != null && !this.numeroContrato.equals(other.numeroContrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.prov.evaluacion.modelo.Contratante[ numeroContrato=" + numeroContrato + " ]";
    }
    
}
