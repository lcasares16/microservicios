/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.usuario;

import com.prov.evaluacion.modelo.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author roberto.espinoza
 */

public interface RepoUsuario extends JpaRepository<Usuarios, Long>{
    
    @Query("SELECT u FROM Usuarios u WHERE u.username = :usuario and u.password = :clave")
    Usuarios find (@Param("usuario") String usuario,
                  @Param("clave") String clave);
    
}
