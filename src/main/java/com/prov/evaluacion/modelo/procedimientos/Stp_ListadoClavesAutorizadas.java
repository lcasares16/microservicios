/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo.procedimientos;
import java.io.Serializable;
/**
 *
 * @author admoracle
 */
public class Stp_ListadoClavesAutorizadas {
         
         private String ID_RECLAMO;
         private String SUB_CLASIFICACION;
         private String FECHA_OCURRENCIA;
         private String STATUS;
         private String MONTO_FACTURADO;
        
         
         
         
         
           public Stp_ListadoClavesAutorizadas () {}
    
    public String getID_RECLAMO() {
        return ID_RECLAMO;
    }

      public void setID_RECLAMO(String ID_RECLAMO) {
        this.ID_RECLAMO = ID_RECLAMO;
       
    }
        
       public String getSUB_CLASIFICACION() {
        return SUB_CLASIFICACION;
    }

 
    public void setSUB_CLASIFICACION(String SUB_CLASIFICACION) {
        this.SUB_CLASIFICACION = SUB_CLASIFICACION;
    }
    
    
       public String getFECHA_OCURRENCIA() {
        return FECHA_OCURRENCIA;
    }

        public void setFECHA_OCURRENCIA(String FECHA_OCURRENCIA) {
        this.FECHA_OCURRENCIA = FECHA_OCURRENCIA;
    }
    
    
       
       
       public String getSTATUS() {
        return STATUS;
    }

       public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
      
    
    public String getMONTO_FACTURADO() {
        return MONTO_FACTURADO;
    }

 
    public void setMONTO_FACTURADO(String MONTO_FACTURADO) {
        this.MONTO_FACTURADO = MONTO_FACTURADO;
    }
 
}
