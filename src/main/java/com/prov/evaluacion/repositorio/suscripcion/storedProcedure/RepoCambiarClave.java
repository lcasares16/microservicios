/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberto.espinoza
 */

@Repository

public class RepoCambiarClave {
    
    @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PCK_CONSULT_LOGIN")
                .withProcedureName("cambiarclave")
                .declareParameters(
                    new SqlParameter("V_ELOGIN", Types.VARCHAR),
                    new SqlParameter("V_ECLAVEUSUARIO", Types.VARCHAR),
                    new SqlParameter("V_ENUMEROCELULAR", Types.VARCHAR),
                    new SqlParameter("v_evalidaprimera", Types.VARCHAR),
                    new SqlOutParameter("mensaje11", Types.VARCHAR)
                );
//                .returningResultSet("MENS1", Be
//                        anPropertyRowMapper.newInstance(String.class));
    }
        
    public String cambiarclave(String login, String clave, String celular, String valida) {
        
//        StoredProcedure storeProcedure = new GenericStoredProcedure();
//        
//        storeProcedure.setDataSource(dataSource);
//        storeProcedure.setSql("PCK_CONSULT_LOGIN.AUTENTICARUSUARIO");
//        storeProcedure.setFunction(false);
//        
//        SqlParameter[] parameters = { 
//            new SqlParameter(Types.VARCHAR),
//            new SqlParameter(Types.VARCHAR),
//            new SqlParameter(Types.VARCHAR),
//            new SqlOutParameter("mens1", Types.VARCHAR),
//            new SqlOutParameter("mens2", Types.VARCHAR),
//            new SqlOutParameter("v_fecha", Types.VARCHAR),
//            new SqlOutParameter("c_autenticarusuario", Types.REF_CURSOR),
//        };
//        
//        storeProcedure.setParameters(parameters);
//        storeProcedure.compile();
//        
//////////////////////////////////////////////////////////////////////////////////////////////


//       Map<String, Object> result = simpleJdbcCall.execute(new MapSqlParameterSource("V_ELOGIN", login.trim()),
//                                                           new MapSqlParameterSource("V_ECLAVEUSUARIO", clave.trim()),
//                                                           new MapSqlParameterSource("V_ENUMEROCELULAR", celular.trim()));
       Map<String, Object> result = simpleJdbcCall.execute(login.trim(),
                                                           clave.trim(),
                                                           celular.trim(),
                                                           valida.trim());
       
       String resultado = (String) result.get("mensaje11");
       return resultado;
////////////////////////////////////////////////////////////////////////////////////////




//        Map<String, Object> result = simpleJdbcCall.execute(new MapSqlParameterSource(paramMap));
//        
//        Iterator<Entry<String, Object>> it = result.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry<String, Object> entry = (Map.Entry<String, Object>) it.next();
//            String key = (String) entry.getKey();
//            Object value = (Object) entry.getValue();
//            System.out.println("Key: "+key);
//            System.out.println("Value: "+value);
//        }
//        
//        
//        RespuestaStandard respuestaStandard = new RespuestaStandard();
//        
//        respuestaStandard.setMENS1((String) result.get("mens1"));
//        respuestaStandard.setMENS2((String) result.get("mens2"));
//        respuestaStandard.setV_FECHA((String) result.get("v_fecha"));
//        respuestaStandard.setRespuestaCursor((List) result.get("c_autenticarusuario"));
//        
//        return respuestaStandard;
    }
    
    
}
