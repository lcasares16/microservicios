/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import com.prov.evaluacion.modelo.procedimientos.StpListadoFacturasContratantes;
import com.prov.evaluacion.modelo.procedimientos.Stp_ListadoClavesAutorizadas;
import com.prov.evaluacion.modelo.procedimientos.Stp_ListadoClavesEmergencia;
import com.prov.evaluacion.modelo.procedimientos.Stp_listadoBaremoaps;
import java.math.BigInteger;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.object.GenericStoredProcedure;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Repository;

@Repository


public class Web_ListadoClavesEmergencia {
   @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PCK_WEB_ClaveEmergencia")
                .withProcedureName("ListadoClavesEmergencia")
                .declareParameters(
                    new SqlParameter("V_CODIGO_CLINICA", Types.CHAR),
                    new SqlOutParameter("rest_cursor", Types.REF_CURSOR))
                .returningResultSet("rest_cursor", BeanPropertyRowMapper.newInstance(Stp_ListadoClavesEmergencia.class));
    }
    
    public List<Stp_ListadoClavesEmergencia> listadoclavesemergencia(String certificado) {
        
        Map<String, Object> result = simpleJdbcCall.execute(new MapSqlParameterSource("V_CODIGO_CLINICA", certificado));
        
        return (List) result.get("rest_cursor");
    } 
}
