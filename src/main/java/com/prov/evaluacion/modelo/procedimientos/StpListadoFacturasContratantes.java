/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo.procedimientos;
import java.io.Serializable;
/**
 *
 * @author admoracle
 */
 
    
public class StpListadoFacturasContratantes implements Serializable   {  
    
    
    
     private String NUMERO_CONTRATO;
    private String NOMBRE;
    
    
     public StpListadoFacturasContratantes () {}
    
    public String getNUMERO_CONTRATO() {
        return NUMERO_CONTRATO;
    }

      public void setNUMERO_CONTRATO(String NUMERO_CONTRATO) {
        this.NUMERO_CONTRATO = NUMERO_CONTRATO;
       
    }
        
       public String getNOMBRE() {
        return NOMBRE;
    }

    /**
     * @param NOMBRE the NOMBRE to set
     */
    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
}
}
