/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
@Repository


public class Web_ValidarCumuloApsPB {
   @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    
    
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PCK_WEB_ClaveAPS")
                .withProcedureName("ValidarCumuloApsPB")
                .declareParameters(
                    new SqlParameter("V_eCodigoSuscripcion", Types.INTEGER),
                         new SqlParameter("V_sMtoCoberturaAPS", Types.VARCHAR),
                          new SqlParameter("V_eMontoTotalPantalla", Types.VARCHAR),
                          new SqlParameter("v_eNumeroContrato", Types.VARCHAR),
                    new SqlOutParameter("mensaje11", Types.VARCHAR)
                );
    
}
    public String validarcumuloapspb(String V_eCodigoSuscripcion, String V_sMtoCoberturaAPS, String V_eMontoTotalPantalla,String v_eNumeroContrato) {
            
      // map<Integer, object> result=  simpleJdbcCall.execute(jcedula.trim());
       Map<String, Object> result = simpleJdbcCall.execute(V_eCodigoSuscripcion.trim(),
                                                           V_sMtoCoberturaAPS.trim(),
                                                           V_eMontoTotalPantalla.trim(),
                                                           v_eNumeroContrato.trim()
                                                            );
                                                          
       String resultado = (String) result.get("mensaje11");
       return resultado;
    }   
}
