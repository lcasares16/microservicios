/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
@Repository


public class Web_GrabarReclamo {
   @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    
    
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("PCK_WEB_ClaveAPS")
                .withProcedureName("GrabarReclamo")
                .declareParameters(
                        new SqlParameter("V_eIdReclamo", Types.INTEGER),
                        new SqlParameter("v_total_facturado", Types.VARCHAR),
                        new SqlParameter("V_eCodigoSuscripcion", Types.INTEGER),
                        new SqlParameter("V_eCodigoTipoServicio", Types.VARCHAR),
                        new SqlParameter("v_codigo_medico", Types.VARCHAR),
                        new SqlParameter("v_codigo_clinica", Types.INTEGER),
                        new SqlParameter("v_status", Types.VARCHAR),
                        new SqlParameter("v_contacto_clinica", Types.VARCHAR),
                        new SqlParameter("v_comentarios", Types.VARCHAR),
                        new SqlParameter("V_eCodAreaCelular", Types.VARCHAR),
                        new SqlParameter("V_eCelular", Types.VARCHAR),
                        new SqlParameter("V_eFechaOcurrencia", Types.VARCHAR),
                        new SqlParameter("v_numero_contrato", Types.VARCHAR),
                        new SqlParameter("v_codigo_intervencion", Types.VARCHAR),
                        new SqlParameter("v_gastos_clinica", Types.VARCHAR),
                        new SqlParameter("v_monto_facturado", Types.VARCHAR),
                        new SqlParameter("v_autorizado", Types.VARCHAR),
                         new SqlParameter("v_clasificacion_e", Types.VARCHAR),
                        new SqlOutParameter("mensaje11", Types.VARCHAR)
                );
    
}
    public String grabarreclamo   (String v_idreclamo,String v_total_facturado, String v_ecodigosuscripcion ,
                                   String V_ecodigotiposervicio, String v_codigo_medico, String v_codigo_clinica,String v_status,	 
                                   String v_contacto_clinica, String v_comentarios,String v_ecodareacelular,String v_ecelular,	 
                                   String v_efechaocurrencia, String v_numero_contrato, String v_codigo_intervencion, String v_gastos_clinica,
                                   String v_monto_facturado,  String v_autorizado, String v_clasificacion_e )
    
    {
       
       // (String jcedula,String jcontrato) {
        
      // map<Integer, object> result=  simpleJdbcCall.execute(jcedula.trim());
       Map<String, Object> result = simpleJdbcCall.execute(v_idreclamo.trim(),v_total_facturado.trim(), v_ecodigosuscripcion.trim() ,
                                                            V_ecodigotiposervicio.trim(), v_codigo_medico.trim(), v_codigo_clinica.trim(),v_status.trim(),	 
                                                            v_contacto_clinica.trim(), v_comentarios.trim(),v_ecodareacelular.trim(),v_ecelular.trim(),	 
                                                            v_efechaocurrencia.trim(), v_numero_contrato.trim(), v_codigo_intervencion.trim(), v_gastos_clinica.trim() , 
                                                            v_monto_facturado.trim(),v_autorizado.trim(),v_clasificacion_e.trim()
                                                              );
       String resultado = (String) result.get("mensaje11");
       return resultado;
    } 
}
