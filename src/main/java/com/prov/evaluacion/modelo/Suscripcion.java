/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author roberto.espinoza
 */
@Entity
@Table(name = "SUSCRIPCION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Suscripcion.findAll", query = "SELECT s FROM Suscripcion s")
    , @NamedQuery(name = "Suscripcion.findByCodigoLocalidad", query = "SELECT s FROM Suscripcion s WHERE s.codigoLocalidad = :codigoLocalidad")
    , @NamedQuery(name = "Suscripcion.findByPlan", query = "SELECT s FROM Suscripcion s WHERE s.plan = :plan")
    , @NamedQuery(name = "Suscripcion.findByFechaInicio", query = "SELECT s FROM Suscripcion s WHERE s.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "Suscripcion.findByCodigoPrima", query = "SELECT s FROM Suscripcion s WHERE s.codigoPrima = :codigoPrima")
    , @NamedQuery(name = "Suscripcion.findByCodigo", query = "SELECT s FROM Suscripcion s WHERE s.codigo = :codigo")
    , @NamedQuery(name = "Suscripcion.findByCertificado", query = "SELECT s FROM Suscripcion s WHERE s.certificado = :certificado")
    , @NamedQuery(name = "Suscripcion.findByNombre", query = "SELECT s FROM Suscripcion s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Suscripcion.findByFechaIngreso", query = "SELECT s FROM Suscripcion s WHERE s.fechaIngreso = :fechaIngreso")
    , @NamedQuery(name = "Suscripcion.findByFechaNacimiento", query = "SELECT s FROM Suscripcion s WHERE s.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Suscripcion.findBySexo", query = "SELECT s FROM Suscripcion s WHERE s.sexo = :sexo")
    , @NamedQuery(name = "Suscripcion.findByEstadoCivil", query = "SELECT s FROM Suscripcion s WHERE s.estadoCivil = :estadoCivil")
    , @NamedQuery(name = "Suscripcion.findByParentesco", query = "SELECT s FROM Suscripcion s WHERE s.parentesco = :parentesco")
    , @NamedQuery(name = "Suscripcion.findByStatus", query = "SELECT s FROM Suscripcion s WHERE s.status = :status")
    , @NamedQuery(name = "Suscripcion.findByFechaRegistro", query = "SELECT s FROM Suscripcion s WHERE s.fechaRegistro = :fechaRegistro")
    , @NamedQuery(name = "Suscripcion.findByPlazosEspera", query = "SELECT s FROM Suscripcion s WHERE s.plazosEspera = :plazosEspera")
    , @NamedQuery(name = "Suscripcion.findByPolizaExceso", query = "SELECT s FROM Suscripcion s WHERE s.polizaExceso = :polizaExceso")
    , @NamedQuery(name = "Suscripcion.findByCodigoFilial", query = "SELECT s FROM Suscripcion s WHERE s.codigoFilial = :codigoFilial")
    , @NamedQuery(name = "Suscripcion.findByCedula", query = "SELECT s FROM Suscripcion s WHERE s.cedula = :cedula")
    , @NamedQuery(name = "Suscripcion.findByFechaEgreso", query = "SELECT s FROM Suscripcion s WHERE s.fechaEgreso = :fechaEgreso")
    , @NamedQuery(name = "Suscripcion.findByCodigoEmpleado", query = "SELECT s FROM Suscripcion s WHERE s.codigoEmpleado = :codigoEmpleado")
    , @NamedQuery(name = "Suscripcion.findByAseguradoraExceso", query = "SELECT s FROM Suscripcion s WHERE s.aseguradoraExceso = :aseguradoraExceso")
    , @NamedQuery(name = "Suscripcion.findByNumeroPolizaExceso", query = "SELECT s FROM Suscripcion s WHERE s.numeroPolizaExceso = :numeroPolizaExceso")
    , @NamedQuery(name = "Suscripcion.findByTipoDeEmpleado", query = "SELECT s FROM Suscripcion s WHERE s.tipoDeEmpleado = :tipoDeEmpleado")
    , @NamedQuery(name = "Suscripcion.findByHechoPor", query = "SELECT s FROM Suscripcion s WHERE s.hechoPor = :hechoPor")
    , @NamedQuery(name = "Suscripcion.findByObservaciones", query = "SELECT s FROM Suscripcion s WHERE s.observaciones = :observaciones")
    , @NamedQuery(name = "Suscripcion.findByPlanExceso", query = "SELECT s FROM Suscripcion s WHERE s.planExceso = :planExceso")
    , @NamedQuery(name = "Suscripcion.findByFechaInicioExceso", query = "SELECT s FROM Suscripcion s WHERE s.fechaInicioExceso = :fechaInicioExceso")
    , @NamedQuery(name = "Suscripcion.findByCodigoPrimaExceso", query = "SELECT s FROM Suscripcion s WHERE s.codigoPrimaExceso = :codigoPrimaExceso")
    , @NamedQuery(name = "Suscripcion.findBySueldo", query = "SELECT s FROM Suscripcion s WHERE s.sueldo = :sueldo")
    , @NamedQuery(name = "Suscripcion.findByPlazosDeEsperaEzceso", query = "SELECT s FROM Suscripcion s WHERE s.plazosDeEsperaEzceso = :plazosDeEsperaEzceso")
    , @NamedQuery(name = "Suscripcion.findByObservacionesExceso", query = "SELECT s FROM Suscripcion s WHERE s.observacionesExceso = :observacionesExceso")
    , @NamedQuery(name = "Suscripcion.findByRevisarBasico", query = "SELECT s FROM Suscripcion s WHERE s.revisarBasico = :revisarBasico")
    , @NamedQuery(name = "Suscripcion.findByRevisarExceso", query = "SELECT s FROM Suscripcion s WHERE s.revisarExceso = :revisarExceso")
    , @NamedQuery(name = "Suscripcion.findByFechaIngresoExceso", query = "SELECT s FROM Suscripcion s WHERE s.fechaIngresoExceso = :fechaIngresoExceso")
    , @NamedQuery(name = "Suscripcion.findByFechaEgresoExceso", query = "SELECT s FROM Suscripcion s WHERE s.fechaEgresoExceso = :fechaEgresoExceso")
    , @NamedQuery(name = "Suscripcion.findByProrrata", query = "SELECT s FROM Suscripcion s WHERE s.prorrata = :prorrata")
    , @NamedQuery(name = "Suscripcion.findByCmf", query = "SELECT s FROM Suscripcion s WHERE s.cmf = :cmf")
    , @NamedQuery(name = "Suscripcion.findByRl", query = "SELECT s FROM Suscripcion s WHERE s.rl = :rl")
    , @NamedQuery(name = "Suscripcion.findByDireccion", query = "SELECT s FROM Suscripcion s WHERE s.direccion = :direccion")
    , @NamedQuery(name = "Suscripcion.findByMaternidad", query = "SELECT s FROM Suscripcion s WHERE s.maternidad = :maternidad")
    , @NamedQuery(name = "Suscripcion.findByExclusionEnfermedades", query = "SELECT s FROM Suscripcion s WHERE s.exclusionEnfermedades = :exclusionEnfermedades")
    , @NamedQuery(name = "Suscripcion.findByStsProcesado", query = "SELECT s FROM Suscripcion s WHERE s.stsProcesado = :stsProcesado")
    , @NamedQuery(name = "Suscripcion.findByCuentaBancaria", query = "SELECT s FROM Suscripcion s WHERE s.cuentaBancaria = :cuentaBancaria")
    , @NamedQuery(name = "Suscripcion.findByCup", query = "SELECT s FROM Suscripcion s WHERE s.cup = :cup")
    , @NamedQuery(name = "Suscripcion.findByStsProcesadoExc", query = "SELECT s FROM Suscripcion s WHERE s.stsProcesadoExc = :stsProcesadoExc")
    , @NamedQuery(name = "Suscripcion.findByCentroCosto", query = "SELECT s FROM Suscripcion s WHERE s.centroCosto = :centroCosto")
    , @NamedQuery(name = "Suscripcion.findByStsProcesadoEgreso", query = "SELECT s FROM Suscripcion s WHERE s.stsProcesadoEgreso = :stsProcesadoEgreso")
    , @NamedQuery(name = "Suscripcion.findByStsProcesadoEgresoExc", query = "SELECT s FROM Suscripcion s WHERE s.stsProcesadoEgresoExc = :stsProcesadoEgresoExc")
    , @NamedQuery(name = "Suscripcion.findByNumeroPoliza", query = "SELECT s FROM Suscripcion s WHERE s.numeroPoliza = :numeroPoliza")
    , @NamedQuery(name = "Suscripcion.findByOdontologico", query = "SELECT s FROM Suscripcion s WHERE s.odontologico = :odontologico")
    , @NamedQuery(name = "Suscripcion.findByModificadoPor", query = "SELECT s FROM Suscripcion s WHERE s.modificadoPor = :modificadoPor")
    , @NamedQuery(name = "Suscripcion.findByFechaModificacion", query = "SELECT s FROM Suscripcion s WHERE s.fechaModificacion = :fechaModificacion")
    , @NamedQuery(name = "Suscripcion.findByMaquina", query = "SELECT s FROM Suscripcion s WHERE s.maquina = :maquina")
    , @NamedQuery(name = "Suscripcion.findByFechaModif", query = "SELECT s FROM Suscripcion s WHERE s.fechaModif = :fechaModif")
    , @NamedQuery(name = "Suscripcion.findByFicha", query = "SELECT s FROM Suscripcion s WHERE s.ficha = :ficha")
    , @NamedQuery(name = "Suscripcion.findByCongenita", query = "SELECT s FROM Suscripcion s WHERE s.congenita = :congenita")
    , @NamedQuery(name = "Suscripcion.findByPreExistente", query = "SELECT s FROM Suscripcion s WHERE s.preExistente = :preExistente")
    , @NamedQuery(name = "Suscripcion.findByCodigoOdontologico", query = "SELECT s FROM Suscripcion s WHERE s.codigoOdontologico = :codigoOdontologico")
    , @NamedQuery(name = "Suscripcion.findByAmbulancia", query = "SELECT s FROM Suscripcion s WHERE s.ambulancia = :ambulancia")
    , @NamedQuery(name = "Suscripcion.findByCodigoAmbulancia", query = "SELECT s FROM Suscripcion s WHERE s.codigoAmbulancia = :codigoAmbulancia")
    , @NamedQuery(name = "Suscripcion.findByAsistencia", query = "SELECT s FROM Suscripcion s WHERE s.asistencia = :asistencia")
    , @NamedQuery(name = "Suscripcion.findByCodigoAsistencia", query = "SELECT s FROM Suscripcion s WHERE s.codigoAsistencia = :codigoAsistencia")
    , @NamedQuery(name = "Suscripcion.findByBonoPlanIncentivo", query = "SELECT s FROM Suscripcion s WHERE s.bonoPlanIncentivo = :bonoPlanIncentivo")
    , @NamedQuery(name = "Suscripcion.findByAutRen", query = "SELECT s FROM Suscripcion s WHERE s.autRen = :autRen")
    , @NamedQuery(name = "Suscripcion.findByDescuento", query = "SELECT s FROM Suscripcion s WHERE s.descuento = :descuento")
    , @NamedQuery(name = "Suscripcion.findByAnexoVida", query = "SELECT s FROM Suscripcion s WHERE s.anexoVida = :anexoVida")
    , @NamedQuery(name = "Suscripcion.findByCodigoAnexoVida", query = "SELECT s FROM Suscripcion s WHERE s.codigoAnexoVida = :codigoAnexoVida")
    , @NamedQuery(name = "Suscripcion.findByRenovar", query = "SELECT s FROM Suscripcion s WHERE s.renovar = :renovar")
    , @NamedQuery(name = "Suscripcion.findByUsuarioRenovacion", query = "SELECT s FROM Suscripcion s WHERE s.usuarioRenovacion = :usuarioRenovacion")
    , @NamedQuery(name = "Suscripcion.findByObservacionesRenovacion", query = "SELECT s FROM Suscripcion s WHERE s.observacionesRenovacion = :observacionesRenovacion")
    , @NamedQuery(name = "Suscripcion.findByEmail", query = "SELECT s FROM Suscripcion s WHERE s.email = :email")
    , @NamedQuery(name = "Suscripcion.findByIdMasivo", query = "SELECT s FROM Suscripcion s WHERE s.idMasivo = :idMasivo")
    , @NamedQuery(name = "Suscripcion.findByMatExceso", query = "SELECT s FROM Suscripcion s WHERE s.matExceso = :matExceso")
    , @NamedQuery(name = "Suscripcion.findByEnfermedadCronica", query = "SELECT s FROM Suscripcion s WHERE s.enfermedadCronica = :enfermedadCronica")
    , @NamedQuery(name = "Suscripcion.findByModuloCronico", query = "SELECT s FROM Suscripcion s WHERE s.moduloCronico = :moduloCronico")
    , @NamedQuery(name = "Suscripcion.findByMatExceso2", query = "SELECT s FROM Suscripcion s WHERE s.matExceso2 = :matExceso2")
    , @NamedQuery(name = "Suscripcion.findByCodigoMedicoMilRazones", query = "SELECT s FROM Suscripcion s WHERE s.codigoMedicoMilRazones = :codigoMedicoMilRazones")
    , @NamedQuery(name = "Suscripcion.findBySumaMaternidad", query = "SELECT s FROM Suscripcion s WHERE s.sumaMaternidad = :sumaMaternidad")
    , @NamedQuery(name = "Suscripcion.findByNacionalidad", query = "SELECT s FROM Suscripcion s WHERE s.nacionalidad = :nacionalidad")
    , @NamedQuery(name = "Suscripcion.findByCodArea", query = "SELECT s FROM Suscripcion s WHERE s.codArea = :codArea")
    , @NamedQuery(name = "Suscripcion.findByCelular", query = "SELECT s FROM Suscripcion s WHERE s.celular = :celular")
    , @NamedQuery(name = "Suscripcion.findByFIngMatExc", query = "SELECT s FROM Suscripcion s WHERE s.fIngMatExc = :fIngMatExc")
    , @NamedQuery(name = "Suscripcion.findBySumaMaternidadExc", query = "SELECT s FROM Suscripcion s WHERE s.sumaMaternidadExc = :sumaMaternidadExc")
    , @NamedQuery(name = "Suscripcion.findByCodigoExclusion", query = "SELECT s FROM Suscripcion s WHERE s.codigoExclusion = :codigoExclusion")})
public class Suscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 10)
    @Column(name = "CODIGO_LOCALIDAD")
    private String codigoLocalidad;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 3)
    @Column(name = "PLAN")
    private String plan;
    @Column(name = "FECHA_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "CODIGO_PRIMA")
    private Long codigoPrima;
    @Id
    @Basic(optional = false)
    //@NotNull
    @Column(name = "CODIGO")
    private Long codigo;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "CERTIFICADO")
    private BigInteger certificado;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 250)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "FECHA_INGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "FECHA_NACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "SEXO")
    private Character sexo;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 10)
    @Column(name = "ESTADO_CIVIL")
    private String estadoCivil;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 10)
    @Column(name = "PARENTESCO")
    private String parentesco;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 20)
    @Column(name = "STATUS")
    private String status;
    @Basic(optional = false)
    //@NotNull
    @Column(name = "FECHA_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 2)
    @Column(name = "PLAZOS_ESPERA")
    private String plazosEspera;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 2)
    @Column(name = "POLIZA_EXCESO")
    private String polizaExceso;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 6)
    @Column(name = "CODIGO_FILIAL")
    private String codigoFilial;
    @Column(name = "CEDULA")
    private BigInteger cedula;
    @Column(name = "FECHA_EGRESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEgreso;
    //@Size(max = 20)
    @Column(name = "CODIGO_EMPLEADO")
    private String codigoEmpleado;
    //@Size(max = 50)
    @Column(name = "ASEGURADORA_EXCESO")
    private String aseguradoraExceso;
    //@Size(max = 10)
    @Column(name = "NUMERO_POLIZA_EXCESO")
    private String numeroPolizaExceso;
    //@Size(max = 100)
    @Column(name = "TIPO_DE_EMPLEADO")
    private String tipoDeEmpleado;
    //@Size(max = 30)
    @Column(name = "HECHO_POR")
    private String hechoPor;
    //@Size(max = 2000)
    @Column(name = "OBSERVACIONES")
    private String observaciones;
    //@Size(max = 3)
    @Column(name = "PLAN_EXCESO")
    private String planExceso;
    @Column(name = "FECHA_INICIO_EXCESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicioExceso;
    @Column(name = "CODIGO_PRIMA_EXCESO")
    private Long codigoPrimaExceso;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUELDO")
    private BigDecimal sueldo;
    //@Size(max = 2)
    @Column(name = "PLAZOS_DE_ESPERA_EZCESO")
    private String plazosDeEsperaEzceso;
    //@Size(max = 2000)
    @Column(name = "OBSERVACIONES_EXCESO")
    private String observacionesExceso;
    //@Size(max = 2)
    @Column(name = "REVISAR_BASICO")
    private String revisarBasico;
    //@Size(max = 2)
    @Column(name = "REVISAR_EXCESO")
    private String revisarExceso;
    @Column(name = "FECHA_INGRESO_EXCESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngresoExceso;
    @Column(name = "FECHA_EGRESO_EXCESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEgresoExceso;
    @Column(name = "PRORRATA")
    private BigDecimal prorrata;
    //@Size(max = 100)
    @Column(name = "CMF")
    private String cmf;
    //@Size(max = 50)
    @Column(name = "RL")
    private String rl;
    //@Size(max = 2000)
    @Column(name = "DIRECCION")
    private String direccion;
    //@Size(max = 2)
    @Column(name = "MATERNIDAD")
    private String maternidad;
    //@Size(max = 1000)
    @Column(name = "EXCLUSION_ENFERMEDADES")
    private String exclusionEnfermedades;
    //@Size(max = 2)
    @Column(name = "STS_PROCESADO")
    private String stsProcesado;
    //@Size(max = 20)
    @Column(name = "CUENTA_BANCARIA")
    private String cuentaBancaria;
    @Column(name = "CUP")
    private BigDecimal cup;
    //@Size(max = 2)
    @Column(name = "STS_PROCESADO_EXC")
    private String stsProcesadoExc;
    //@Size(max = 15)
    @Column(name = "CENTRO_COSTO")
    private String centroCosto;
    //@Size(max = 2)
    @Column(name = "STS_PROCESADO_EGRESO")
    private String stsProcesadoEgreso;
    //@Size(max = 2)
    @Column(name = "STS_PROCESADO_EGRESO_EXC")
    private String stsProcesadoEgresoExc;
    //@Size(max = 20)
    @Column(name = "NUMERO_POLIZA")
    private String numeroPoliza;
    //@Size(max = 2)
    @Column(name = "ODONTOLOGICO")
    private String odontologico;
    //@Size(max = 30)
    @Column(name = "MODIFICADO_POR")
    private String modificadoPor;
    @Column(name = "FECHA_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    //@Size(max = 30)
    @Column(name = "MAQUINA")
    private String maquina;
    //@Size(max = 20)
    @Column(name = "FECHA_MODIF")
    private String fechaModif;
    //@Size(max = 20)
    @Column(name = "FICHA")
    private String ficha;
    //@Size(max = 2)
    @Column(name = "CONGENITA")
    private String congenita;
    //@Size(max = 2)
    @Column(name = "PRE_EXISTENTE")
    private String preExistente;
    @Column(name = "CODIGO_ODONTOLOGICO")
    private Long codigoOdontologico;
    //@Size(max = 2)
    @Column(name = "AMBULANCIA")
    private String ambulancia;
    @Column(name = "CODIGO_AMBULANCIA")
    private Long codigoAmbulancia;
    //@Size(max = 2)
    @Column(name = "ASISTENCIA")
    private String asistencia;
    @Column(name = "CODIGO_ASISTENCIA")
    private Long codigoAsistencia;
    //@Size(max = 2)
    @Column(name = "BONO_PLAN_INCENTIVO")
    private String bonoPlanIncentivo;
    //@Size(max = 2)
    @Column(name = "AUT_REN")
    private String autRen;
    @Column(name = "DESCUENTO")
    private Short descuento;
    //@Size(max = 2)
    @Column(name = "ANEXO_VIDA")
    private String anexoVida;
    @Column(name = "CODIGO_ANEXO_VIDA")
    private Short codigoAnexoVida;
    //@Size(max = 2)
    @Column(name = "RENOVAR")
    private String renovar;
    //@Size(max = 30)
    @Column(name = "USUARIO_RENOVACION")
    private String usuarioRenovacion;
    //@Size(max = 2000)
    @Column(name = "OBSERVACIONES_RENOVACION")
    private String observacionesRenovacion;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    //@Size(max = 50)
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "ID_MASIVO")
    private Long idMasivo;
    //@Size(max = 2)
    @Column(name = "MAT_EXCESO")
    private String matExceso;
    //@Size(max = 2)
    @Column(name = "ENFERMEDAD_CRONICA")
    private String enfermedadCronica;
    //@Size(max = 2)
    @Column(name = "MODULO_CRONICO")
    private String moduloCronico;
    //@Size(max = 2)
    @Column(name = "MAT_EXCESO2")
    private String matExceso2;
    //@Size(max = 4)
    @Column(name = "CODIGO_MEDICO_MIL_RAZONES")
    private String codigoMedicoMilRazones;
    @Column(name = "SUMA_MATERNIDAD")
    private BigDecimal sumaMaternidad;
    @Column(name = "NACIONALIDAD")
    private Character nacionalidad;
    //@Size(max = 4)
    @Column(name = "COD_AREA")
    private String codArea;
    //@Size(max = 15)
    @Column(name = "CELULAR")
    private String celular;
    @Column(name = "F_ING_MAT_EXC")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fIngMatExc;
    @Column(name = "SUMA_MATERNIDAD_EXC")
    private BigDecimal sumaMaternidadExc;
    //@Size(max = 4)
    @Column(name = "CODIGO_EXCLUSION")
    private String codigoExclusion;
    @JoinColumn(name = "NUMERO_CONTRATO", referencedColumnName = "NUMERO_CONTRATO")
    @ManyToOne(optional = false)
    private Contratante numeroContrato;

    public Suscripcion() {
    }

    public Suscripcion(Long codigo) {
        this.codigo = codigo;
    }

    public Suscripcion(Long codigo, String codigoLocalidad, String plan, BigInteger certificado, String nombre, Date fechaIngreso, Date fechaNacimiento, Character sexo, String estadoCivil, String parentesco, String status, Date fechaRegistro, String plazosEspera, String polizaExceso, String codigoFilial) {
        this.codigo = codigo;
        this.codigoLocalidad = codigoLocalidad;
        this.plan = plan;
        this.certificado = certificado;
        this.nombre = nombre;
        this.fechaIngreso = fechaIngreso;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.estadoCivil = estadoCivil;
        this.parentesco = parentesco;
        this.status = status;
        this.fechaRegistro = fechaRegistro;
        this.plazosEspera = plazosEspera;
        this.polizaExceso = polizaExceso;
        this.codigoFilial = codigoFilial;
    }

    public String getCodigoLocalidad() {
        return codigoLocalidad;
    }

    public void setCodigoLocalidad(String codigoLocalidad) {
        this.codigoLocalidad = codigoLocalidad;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Long getCodigoPrima() {
        return codigoPrima;
    }

    public void setCodigoPrima(Long codigoPrima) {
        this.codigoPrima = codigoPrima;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public BigInteger getCertificado() {
        return certificado;
    }

    public void setCertificado(BigInteger certificado) {
        this.certificado = certificado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(String parentesco) {
        this.parentesco = parentesco;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getPlazosEspera() {
        return plazosEspera;
    }

    public void setPlazosEspera(String plazosEspera) {
        this.plazosEspera = plazosEspera;
    }

    public String getPolizaExceso() {
        return polizaExceso;
    }

    public void setPolizaExceso(String polizaExceso) {
        this.polizaExceso = polizaExceso;
    }

    public String getCodigoFilial() {
        return codigoFilial;
    }

    public void setCodigoFilial(String codigoFilial) {
        this.codigoFilial = codigoFilial;
    }

    public BigInteger getCedula() {
        return cedula;
    }

    public void setCedula(BigInteger cedula) {
        this.cedula = cedula;
    }

    public Date getFechaEgreso() {
        return fechaEgreso;
    }

    public void setFechaEgreso(Date fechaEgreso) {
        this.fechaEgreso = fechaEgreso;
    }

    public String getCodigoEmpleado() {
        return codigoEmpleado;
    }

    public void setCodigoEmpleado(String codigoEmpleado) {
        this.codigoEmpleado = codigoEmpleado;
    }

    public String getAseguradoraExceso() {
        return aseguradoraExceso;
    }

    public void setAseguradoraExceso(String aseguradoraExceso) {
        this.aseguradoraExceso = aseguradoraExceso;
    }

    public String getNumeroPolizaExceso() {
        return numeroPolizaExceso;
    }

    public void setNumeroPolizaExceso(String numeroPolizaExceso) {
        this.numeroPolizaExceso = numeroPolizaExceso;
    }

    public String getTipoDeEmpleado() {
        return tipoDeEmpleado;
    }

    public void setTipoDeEmpleado(String tipoDeEmpleado) {
        this.tipoDeEmpleado = tipoDeEmpleado;
    }

    public String getHechoPor() {
        return hechoPor;
    }

    public void setHechoPor(String hechoPor) {
        this.hechoPor = hechoPor;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getPlanExceso() {
        return planExceso;
    }

    public void setPlanExceso(String planExceso) {
        this.planExceso = planExceso;
    }

    public Date getFechaInicioExceso() {
        return fechaInicioExceso;
    }

    public void setFechaInicioExceso(Date fechaInicioExceso) {
        this.fechaInicioExceso = fechaInicioExceso;
    }

    public Long getCodigoPrimaExceso() {
        return codigoPrimaExceso;
    }

    public void setCodigoPrimaExceso(Long codigoPrimaExceso) {
        this.codigoPrimaExceso = codigoPrimaExceso;
    }

    public BigDecimal getSueldo() {
        return sueldo;
    }

    public void setSueldo(BigDecimal sueldo) {
        this.sueldo = sueldo;
    }

    public String getPlazosDeEsperaEzceso() {
        return plazosDeEsperaEzceso;
    }

    public void setPlazosDeEsperaEzceso(String plazosDeEsperaEzceso) {
        this.plazosDeEsperaEzceso = plazosDeEsperaEzceso;
    }

    public String getObservacionesExceso() {
        return observacionesExceso;
    }

    public void setObservacionesExceso(String observacionesExceso) {
        this.observacionesExceso = observacionesExceso;
    }

    public String getRevisarBasico() {
        return revisarBasico;
    }

    public void setRevisarBasico(String revisarBasico) {
        this.revisarBasico = revisarBasico;
    }

    public String getRevisarExceso() {
        return revisarExceso;
    }

    public void setRevisarExceso(String revisarExceso) {
        this.revisarExceso = revisarExceso;
    }

    public Date getFechaIngresoExceso() {
        return fechaIngresoExceso;
    }

    public void setFechaIngresoExceso(Date fechaIngresoExceso) {
        this.fechaIngresoExceso = fechaIngresoExceso;
    }

    public Date getFechaEgresoExceso() {
        return fechaEgresoExceso;
    }

    public void setFechaEgresoExceso(Date fechaEgresoExceso) {
        this.fechaEgresoExceso = fechaEgresoExceso;
    }

    public BigDecimal getProrrata() {
        return prorrata;
    }

    public void setProrrata(BigDecimal prorrata) {
        this.prorrata = prorrata;
    }

    public String getCmf() {
        return cmf;
    }

    public void setCmf(String cmf) {
        this.cmf = cmf;
    }

    public String getRl() {
        return rl;
    }

    public void setRl(String rl) {
        this.rl = rl;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getMaternidad() {
        return maternidad;
    }

    public void setMaternidad(String maternidad) {
        this.maternidad = maternidad;
    }

    public String getExclusionEnfermedades() {
        return exclusionEnfermedades;
    }

    public void setExclusionEnfermedades(String exclusionEnfermedades) {
        this.exclusionEnfermedades = exclusionEnfermedades;
    }

    public String getStsProcesado() {
        return stsProcesado;
    }

    public void setStsProcesado(String stsProcesado) {
        this.stsProcesado = stsProcesado;
    }

    public String getCuentaBancaria() {
        return cuentaBancaria;
    }

    public void setCuentaBancaria(String cuentaBancaria) {
        this.cuentaBancaria = cuentaBancaria;
    }

    public BigDecimal getCup() {
        return cup;
    }

    public void setCup(BigDecimal cup) {
        this.cup = cup;
    }

    public String getStsProcesadoExc() {
        return stsProcesadoExc;
    }

    public void setStsProcesadoExc(String stsProcesadoExc) {
        this.stsProcesadoExc = stsProcesadoExc;
    }

    public String getCentroCosto() {
        return centroCosto;
    }

    public void setCentroCosto(String centroCosto) {
        this.centroCosto = centroCosto;
    }

    public String getStsProcesadoEgreso() {
        return stsProcesadoEgreso;
    }

    public void setStsProcesadoEgreso(String stsProcesadoEgreso) {
        this.stsProcesadoEgreso = stsProcesadoEgreso;
    }

    public String getStsProcesadoEgresoExc() {
        return stsProcesadoEgresoExc;
    }

    public void setStsProcesadoEgresoExc(String stsProcesadoEgresoExc) {
        this.stsProcesadoEgresoExc = stsProcesadoEgresoExc;
    }

    public String getNumeroPoliza() {
        return numeroPoliza;
    }

    public void setNumeroPoliza(String numeroPoliza) {
        this.numeroPoliza = numeroPoliza;
    }

    public String getOdontologico() {
        return odontologico;
    }

    public void setOdontologico(String odontologico) {
        this.odontologico = odontologico;
    }

    public String getModificadoPor() {
        return modificadoPor;
    }

    public void setModificadoPor(String modificadoPor) {
        this.modificadoPor = modificadoPor;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public String getMaquina() {
        return maquina;
    }

    public void setMaquina(String maquina) {
        this.maquina = maquina;
    }

    public String getFechaModif() {
        return fechaModif;
    }

    public void setFechaModif(String fechaModif) {
        this.fechaModif = fechaModif;
    }

    public String getFicha() {
        return ficha;
    }

    public void setFicha(String ficha) {
        this.ficha = ficha;
    }

    public String getCongenita() {
        return congenita;
    }

    public void setCongenita(String congenita) {
        this.congenita = congenita;
    }

    public String getPreExistente() {
        return preExistente;
    }

    public void setPreExistente(String preExistente) {
        this.preExistente = preExistente;
    }

    public Long getCodigoOdontologico() {
        return codigoOdontologico;
    }

    public void setCodigoOdontologico(Long codigoOdontologico) {
        this.codigoOdontologico = codigoOdontologico;
    }

    public String getAmbulancia() {
        return ambulancia;
    }

    public void setAmbulancia(String ambulancia) {
        this.ambulancia = ambulancia;
    }

    public Long getCodigoAmbulancia() {
        return codigoAmbulancia;
    }

    public void setCodigoAmbulancia(Long codigoAmbulancia) {
        this.codigoAmbulancia = codigoAmbulancia;
    }

    public String getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String asistencia) {
        this.asistencia = asistencia;
    }

    public Long getCodigoAsistencia() {
        return codigoAsistencia;
    }

    public void setCodigoAsistencia(Long codigoAsistencia) {
        this.codigoAsistencia = codigoAsistencia;
    }

    public String getBonoPlanIncentivo() {
        return bonoPlanIncentivo;
    }

    public void setBonoPlanIncentivo(String bonoPlanIncentivo) {
        this.bonoPlanIncentivo = bonoPlanIncentivo;
    }

    public String getAutRen() {
        return autRen;
    }

    public void setAutRen(String autRen) {
        this.autRen = autRen;
    }

    public Short getDescuento() {
        return descuento;
    }

    public void setDescuento(Short descuento) {
        this.descuento = descuento;
    }

    public String getAnexoVida() {
        return anexoVida;
    }

    public void setAnexoVida(String anexoVida) {
        this.anexoVida = anexoVida;
    }

    public Short getCodigoAnexoVida() {
        return codigoAnexoVida;
    }

    public void setCodigoAnexoVida(Short codigoAnexoVida) {
        this.codigoAnexoVida = codigoAnexoVida;
    }

    public String getRenovar() {
        return renovar;
    }

    public void setRenovar(String renovar) {
        this.renovar = renovar;
    }

    public String getUsuarioRenovacion() {
        return usuarioRenovacion;
    }

    public void setUsuarioRenovacion(String usuarioRenovacion) {
        this.usuarioRenovacion = usuarioRenovacion;
    }

    public String getObservacionesRenovacion() {
        return observacionesRenovacion;
    }

    public void setObservacionesRenovacion(String observacionesRenovacion) {
        this.observacionesRenovacion = observacionesRenovacion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIdMasivo() {
        return idMasivo;
    }

    public void setIdMasivo(Long idMasivo) {
        this.idMasivo = idMasivo;
    }

    public String getMatExceso() {
        return matExceso;
    }

    public void setMatExceso(String matExceso) {
        this.matExceso = matExceso;
    }

    public String getEnfermedadCronica() {
        return enfermedadCronica;
    }

    public void setEnfermedadCronica(String enfermedadCronica) {
        this.enfermedadCronica = enfermedadCronica;
    }

    public String getModuloCronico() {
        return moduloCronico;
    }

    public void setModuloCronico(String moduloCronico) {
        this.moduloCronico = moduloCronico;
    }

    public String getMatExceso2() {
        return matExceso2;
    }

    public void setMatExceso2(String matExceso2) {
        this.matExceso2 = matExceso2;
    }

    public String getCodigoMedicoMilRazones() {
        return codigoMedicoMilRazones;
    }

    public void setCodigoMedicoMilRazones(String codigoMedicoMilRazones) {
        this.codigoMedicoMilRazones = codigoMedicoMilRazones;
    }

    public BigDecimal getSumaMaternidad() {
        return sumaMaternidad;
    }

    public void setSumaMaternidad(BigDecimal sumaMaternidad) {
        this.sumaMaternidad = sumaMaternidad;
    }

    public Character getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(Character nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getCodArea() {
        return codArea;
    }

    public void setCodArea(String codArea) {
        this.codArea = codArea;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Date getFIngMatExc() {
        return fIngMatExc;
    }

    public void setFIngMatExc(Date fIngMatExc) {
        this.fIngMatExc = fIngMatExc;
    }

    public BigDecimal getSumaMaternidadExc() {
        return sumaMaternidadExc;
    }

    public void setSumaMaternidadExc(BigDecimal sumaMaternidadExc) {
        this.sumaMaternidadExc = sumaMaternidadExc;
    }

    public String getCodigoExclusion() {
        return codigoExclusion;
    }

    public void setCodigoExclusion(String codigoExclusion) {
        this.codigoExclusion = codigoExclusion;
    }

    public Contratante getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(Contratante numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suscripcion)) {
            return false;
        }
        Suscripcion other = (Suscripcion) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.prov.evaluacion.modelo.Suscripcion[ codigo=" + codigo + " ]";
    }
    
}
