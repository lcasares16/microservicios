/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion.storedProcedure;

import com.prov.evaluacion.modelo.RespuestaStandard;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
@Repository


public class RepoClavesListadoClaves {
     @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcCall simpleJdbcCall;
    
    @PostConstruct
    
    
    private void postConstruct() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withCatalogName("pck_consult_clave")
                .withProcedureName("listadoclaves")
                .declareParameters(
                    new SqlParameter("v_cod_clinica", Types.INTEGER),
                    new SqlParameter("v_fecha_ini", Types.VARCHAR), 
                    new SqlParameter("v_fecha_fin", Types.VARCHAR), 
                    new SqlParameter("v_status", Types.VARCHAR), 
                    new SqlOutParameter("mensaje11", Types.VARCHAR),
                    new SqlOutParameter("mensaje12", Types.VARCHAR)
                );
    
}
    public String claveslistadoclaves(String jclinica,String jfechaini,String jfechafin,String jstatus ) {
            
      // map<Integer, object> result=  simpleJdbcCall.execute(jcedula.trim());
       Map<String, Object> result = simpleJdbcCall.execute(jclinica.trim(),
                                                           jfechaini.trim(),
                                                           jfechafin.trim(),
                                                           jstatus.trim());
     
         String resultado  = (String) result.get("mensaje11");
        /* String resultado2 = (String) result.get("mensaje12");*/
         
         
       
       return resultado ;
        
         
        
        
    }
      public String claveslistadoclaves2(String jclinica,String jfechaini,String jfechafin,String jstatus ) {
            
      // map<Integer, object> result=  simpleJdbcCall.execute(jcedula.trim());
       Map<String, Object> result1 = simpleJdbcCall.execute(jclinica.trim(),
                                                           jfechaini.trim(),
                                                           jfechafin.trim(),
                                                           jstatus.trim());
     
         String resultado2  = (String) result1.get("mensaje12");
        /* String resultado2 = (String) result.get("mensaje12");*/
         
         
       
       return resultado2 ;
        
         
        
        
    }
}
