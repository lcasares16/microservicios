/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.controlador;

import com.prov.evaluacion.DbContextHolder;
import com.prov.evaluacion.DbType;
import com.prov.evaluacion.modelo.Contratante;
import com.prov.evaluacion.repositorio.contratante.servicio.InterfContratante;
import java.math.BigDecimal;
import java.util.GregorianCalendar;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto.espinoza
 */

@RestController
public class CtrlContratante {
    
    @Autowired
    private InterfContratante interfContratante;
    
    @Autowired
    private EntityManager entityManager;
    
    @PostMapping(path = "/agregarcontratante/{contrato}")
    public void agregarContratante(@PathVariable(value = "contrato") final String contrato) throws Exception {
        
        try {
            DbContextHolder.setDbType(DbType.Oracle);
            
            Contratante conts = interfContratante.buscarPorContrato(contrato);
            
            if (conts instanceof Contratante) {
                throw new Exception(String.format("El contrato %s existe en base de datos", contrato));
            }
            else {
                conts = interfContratante.ultimoContrato();
                entityManager.detach(conts);
                conts.setNumeroContrato(contrato);
            }
            
            interfContratante.salvarContratante(conts);
        }
        finally {
            DbContextHolder.clearDbType(); //Cierra conexion base de datos
        }
        
        
    }
}