/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.suscripcion;

import com.prov.evaluacion.modelo.Suscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
/**
 *
 * @author roberto.espinoza
 */

public interface RepoSuscripcion extends JpaRepository<Suscripcion, Long>{
    
    @Query("select s" +
           "  from Suscripcion s" +
           " where s.codigo = (select max(su.codigo)" +
           "                     from Contratante c, Suscripcion su" +
           "                    where c.numeroContrato = su.numeroContrato" +
           "                      and c.numeroContrato = :numeroContrato)")
    Suscripcion ultimaSuscripcion(@Param("numeroContrato") final String numeroContrato); 
}
