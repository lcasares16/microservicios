/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo.procedimientos;
import java.io.Serializable;

/**
 *
 * @author admoracle
 */
public class Stp_listadoBaremoaps {
         
   
         private String CODIGO_CLINICA;
         private String CODIGO_ESPECIALIDAD;
         private String CODIGO_CLASIFICACION;
         private String CODIGO_SUB_CLASIFICA;
         private String CODIGO_TIPO_SERVICIO;
         private String descripcion_especialidad;
         private String descripcion_clasificacion;
         private String descripcion_sub_clasificacion;
         private String descripcion_tipo_de_servicio;
         private String MONTO_BAREMOS;
         private String COSTOS_BAREMOS;
         private String STATUS;
         private String FECHA_BAREMOS;
         private String TIPO_BAREMO;
         
        
          
         
         
    
     public Stp_listadoBaremoaps () {}
    
    public String getCODIGO_CLINICA() {
        return CODIGO_CLINICA;
    }

      public void setCODIGO_CLINICA(String CODIGO_CLINICA) {
        this.CODIGO_CLINICA = CODIGO_CLINICA;
       
    }
        
       public String getCODIGO_ESPECIALIDAD() {
        return CODIGO_ESPECIALIDAD;
    }

 
    public void setCODIGO_ESPECIALIDAD(String CODIGO_ESPECIALIDAD) {
        this.CODIGO_ESPECIALIDAD = CODIGO_ESPECIALIDAD;
    }
    
    
       public String getCODIGO_CLASIFICACION() {
        return CODIGO_CLASIFICACION;
    }

        public void setCODIGO_CLASIFICACION(String CODIGO_CLASIFICACION) {
        this.CODIGO_CLASIFICACION = CODIGO_CLASIFICACION;
    }
    
    
       
       
       
       
       public String getCODIGO_SUB_CLASIFICA() {
        return CODIGO_SUB_CLASIFICA;
    }

       public void setCODIGO_SUB_CLASIFICA(String CODIGO_SUB_CLASIFICA) {
        this.CODIGO_SUB_CLASIFICA = CODIGO_SUB_CLASIFICA;
    }
      
    
       
       
       
       
    public String getCODIGO_TIPO_SERVICIO() {
        return CODIGO_TIPO_SERVICIO;
    }

 
    public void setCODIGO_TIPO_SERVICIO(String CODIGO_TIPO_SERVICIO) {
        this.CODIGO_TIPO_SERVICIO = CODIGO_TIPO_SERVICIO;
    }
    
    
    
    
       public String getdescripcion_especialidad() {
        return descripcion_especialidad;
    }

 
    public void setdescripcion_especialidad(String descripcion_especialidad) {
        this.descripcion_especialidad = descripcion_especialidad;
    }
    
    
       public String getdescripcion_clasificacion() {
        return descripcion_clasificacion;
    }

 
    public void setdescripcion_clasificacion(String descripcion_clasificacion) {
        this.descripcion_clasificacion = descripcion_clasificacion;
    }
    
    
    
    
       public String getdescripcion_sub_clasificacion() {
        return descripcion_sub_clasificacion;
    }

 
    public void setdescripcion_sub_clasificacion(String descripcion_sub_clasificacion) {
        this.descripcion_sub_clasificacion = descripcion_sub_clasificacion;
    }
    
    
    
       public String getdescripcion_tipo_de_servicio() {
        return descripcion_tipo_de_servicio;
    }

 
    public void setdescripcion_tipo_de_servicio(String descripcion_tipo_de_servicio) {
        this.descripcion_tipo_de_servicio = descripcion_tipo_de_servicio;
    }
    
    
    
       public String getMONTO_BAREMOS() {
        return MONTO_BAREMOS;
    }

 
    public void setMONTO_BAREMOS(String MONTO_BAREMOS) {
        this.MONTO_BAREMOS = MONTO_BAREMOS;
    }
    
    
          public String getCOSTOS_BAREMOS() {
        return COSTOS_BAREMOS;
    }

 
    public void setCOSTOS_BAREMOS(String COSTOS_BAREMOS) {
        this.COSTOS_BAREMOS = COSTOS_BAREMOS;
    }
     
       
    
    public String getSTATUS() {
        return STATUS;
    }

 
    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
    
    
    
    
      public String getFECHA_BAREMOS() {
        return FECHA_BAREMOS;
    }

 
    public void setFECHA_BAREMOS(String FECHA_BAREMOS) {
        this.FECHA_BAREMOS = FECHA_BAREMOS;
    }
    
    
    
      public String getTIPO_BAREMO() {
        return TIPO_BAREMO;
    }

 
    public void setTIPO_BAREMO(String TIPO_BAREMO) {
        this.TIPO_BAREMO = TIPO_BAREMO;
    }
    
    
}
