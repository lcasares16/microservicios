/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.modelo.procedimientos;

import java.io.Serializable;

/**
 *
 * @author roberto.espinoza
 */
public class Stp_pruebaRoberto implements Serializable {

    private Long CEDULA;
    private String NOMBRE;
    private String PARENTESCO;
    private String FECHA_NACIMIENTO;
    private Integer CANTIDADDEFAMILIARES;
    private String NOMBRE2;
    
    public Stp_pruebaRoberto () {}
    
    /**
     * @return the CEDULA
     */
    public Long getCEDULA() {
        return CEDULA;
    }

    /**
     * @param CEDULA the CEDULA to set
     */
    public void setCEDULA(Long CEDULA) {
        this.CEDULA = CEDULA;
    }

    /**
     * @return the NOMBRE
     */
    public String getNOMBRE() {
        return NOMBRE;
    }

    /**
     * @param NOMBRE the NOMBRE to set
     */
    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    /**
     * @return the PARENTESCO
     */
    public String getPARENTESCO() {
        return PARENTESCO;
    }

    /**
     * @param PARENTESCO the PARENTESCO to set
     */
    public void setPARENTESCO(String PARENTESCO) {
        this.PARENTESCO = PARENTESCO;
    }

    /**
     * @return the FECHA_NACIMIENTO
     */
    public String getFECHA_NACIMIENTO() {
        return FECHA_NACIMIENTO;
    }

    /**
     * @param FECHA_NACIMIENTO the FECHA_NACIMIENTO to set
     */
    public void setFECHA_NACIMIENTO(String FECHA_NACIMIENTO) {
        this.FECHA_NACIMIENTO = FECHA_NACIMIENTO;
    }

    /**
     * @return the CANTIDADDEFAMILIARES
     */
    public Integer getCANTIDADDEFAMILIARES() {
        return CANTIDADDEFAMILIARES;
    }

    /**
     * @param CANTIDADDEFAMILIARES the CANTIDADDEFAMILIARES to set
     */
    public void setCANTIDADDEFAMILIARES(Integer CANTIDADDEFAMILIARES) {
        this.CANTIDADDEFAMILIARES = CANTIDADDEFAMILIARES;
    }

    /**
     * @return the NOMBRE2
     */
    public String getNOMBRE2() {
        return NOMBRE2;
    }

    /**
     * @param NOMBRE2 the NOMBRE2 to set
     */
    public void setNOMBRE2(String NOMBRE2) {
        this.NOMBRE2 = NOMBRE2;
    }
}
