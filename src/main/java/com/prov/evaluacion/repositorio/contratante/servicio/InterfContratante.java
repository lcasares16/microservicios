/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prov.evaluacion.repositorio.contratante.servicio;

import com.prov.evaluacion.modelo.Contratante;

/**
 *
 * @author roberto.espinoza
 */
public interface InterfContratante {
    
    public Contratante buscarPorContrato (String contrato);
    
    public Contratante salvarContratante (Contratante contratante);
    
    public Contratante ultimoContrato ();
    
}
